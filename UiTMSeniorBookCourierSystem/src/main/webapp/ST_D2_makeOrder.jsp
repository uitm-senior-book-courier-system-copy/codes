<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int studid = (Integer) session.getAttribute("id");
  String firstname = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  String studgender = (String) session.getAttribute("studgender");
  
  if(role.equalsIgnoreCase("student") != true)	  
      response.sendRedirect("ST_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="Student/sidebar_st.css">
		<link rel="stylesheet" href="Student/header_st.css">
		<link rel="stylesheet" href="Student/body_st.css">
		
		<style>
			body {font-family: "Lato", sans-serif; background: white;}
			
			/*viewDetailsAdsListAD page style*/
			.detail-tablest table {width:100%; border: 0px; border-collapse: collapse;}
			
			.detail-tablest th, .detail-table td {
				padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;
			}
			
			.detail-tablest td { width:80%; }
			.detail-tablest th { width: 10%; color: #000099; padding-right: 20px;}
			
			.adsImg { width:300px; }
			#adsImgBorder{width: 10%; border:0px;}
			#bigImage{width:400px; height:400px;}
			
			.editButton, .backButton {
			  border-radius: 4px; border: none; color: white; padding: 10px 29px; text-align: center;
			  text-decoration: none; display: inline-block; font-size: 17px; cursor: pointer;
			  margin-left:10px;
			}
			
			.editButton {background-color: #365cc7;}
			.editButton:hover {background-color: #26428c; color:white;}
			
			.backButton {background-color: #00cccc;}
			.backButton:hover {background-color: #008080; color:white;}
			
		</style>
		<meta charset="ISO-8859-1">
		<title>Details of Selected Advertisement</title>
	</head>
	<body>
	
		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		  
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>STUDENT</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="ST_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=studentprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist" class="active"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=studorderlist"> View Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
		
		<!-- HEADER -->
		<div id="header">
		
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=firstname %> | <%=role %></span>
		
		</div>
		
		<!-- CONTENT -->
		<div id="body">
		
			<!--Title -->
			<h1 style="color:black;">Details of Selected Advertisement</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>Details of selected advertisement with picture</h3>
			</div>
			
			<hr class="hrStyle"><br>
			
			<!--Body-->
			<div class="detail-tablest">
				<div align="center">
				
					<!-- Display details of selected advertisement -->
					<table>
				
						<tr>
							<td rowspan="4" id="adsImgBorder">
								<c:if test = "${not empty ad.adspicture}">
									<img src="data:image/jpg;base64,${ad.adspicture}" id="bigImage" />
						      	</c:if>
						      	<c:if test = "${empty ad.adspicture}">
						         	<img src="No-Image-Placeholder.png" id="bigImage" />
						      	</c:if>
							</td>
							<th>TITLE</th>
							<td><c:out value="${ad.adstitle}" /></td>
							
						</tr>
						
						<tr>
							<th>SENIOR</th>
							<td>
								<c:out value="${ad.employees.name}"/> (<c:out value="${ad.employees.empgender}"/>)
								<br><c:out value="${ad.employees.empphonenum}"/>
								<br><c:out value="${ad.employees.empemail}"/>
							</td>
						</tr>
						
						<tr>
							<th>DESCRIPTION</th>
							<td><c:out value="${ad.adsdesc}" /></td>
						</tr>
						
						<tr>
							<th>PRICE</th>
							<td>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${ad.price}"/></td>
						</tr>
						
					</table>
					<!------------------------------------------------------>
				</div>
			</div>
			
			<br><br>
			<div align="center">
				<a href = "AdvertisementController?action=adslist">
					<button class="backButton" name="back" title="Button to go back to List of Advertisement">Back</button></a>
				
				<a href="AdvertisementController?action=order&adsid=<c:out value="${ad.adsid}"/>">
					<button class="editButton" name="edit" title="Button to go to Order Advertisement page">Order</button></a>
			</div>
			
		</div><br>
		
		<script>
			//Script to open dan close side menu 
			function openNav() {
			  document.getElementById("mySidenav").style.width = "250px";
			  document.getElementById("header").style.marginLeft = "250px";
			  document.getElementById("body").style.marginLeft="250px";
			}
			
			function closeNav() {
			  document.getElementById("mySidenav").style.width = "0";
			  document.getElementById("header").style.marginLeft= "0";
			  document.getElementById("body").style.marginLeft="0";
			}
		</script>
		
	</body>
</html>