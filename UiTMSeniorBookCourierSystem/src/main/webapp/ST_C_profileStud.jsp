<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int studid = (Integer) session.getAttribute("id");
  String firstname = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  String studgender = (String) session.getAttribute("studgender");
  
  if(role.equalsIgnoreCase("student") != true)	  
      response.sendRedirect("ST_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit Profile</title>
<%
	//Display alert message 
	String alert = (String) request.getAttribute("getAlert");
	
	if(alert != null) 
	{
		if(alert.equalsIgnoreCase("Update Success")) //after add advertisement
		{
			%>
			<script type="text/javascript">
				alert("Profile has been updated successfully");
			</script>
			<%
		}
		if(alert.equalsIgnoreCase("UpdatePs Success")) //after add advertisement
		{
			%>
			<script type="text/javascript">
				alert("Password has been updated successfully");
			</script>
			<%
		}
		if(alert.equalsIgnoreCase("same password")) //after add advertisement
		{
			%>
			<script type="text/javascript">
				alert("You cannot use the old password as the new password!\n\nTry Again.");
			</script>
			<%
		}
	}
%>

	 <link rel="stylesheet" href="Student/sidebar_st.css">
	<link rel="stylesheet" href="Student/header_st.css">
	<link rel="stylesheet" href="Student/body_st.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
	
	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		table,td,tr,th {border:0px;}	
		input[type=email] {
		  width: 20%; padding: 12px 20px; margin: 8px 0; display: inline-block;
		  border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box;
		}
		#updatePs {
			background-color: #4475fc; border-radius: 4px; border: none; color: white; padding: 10px 40px;
			text-align: center; text-decoration: none; display: inline-block; font-size: 17px;
		}
		
	</style>
		
	<script>
	
	$(document).ready(function()
	{
		$('button[name="Update"]').click(function()
		{	
			var m = $("#matricnum").val();
			var c = $("#collegehousenum").val();
			var p = $("#studphonenum").val();
			
			var r_matric = $.isNumeric(m);
			var r_phonenum = $.isNumeric($("#studphonenum").val());
			var r_housenum = $.isNumeric(c);
			
			if (r_matric == true && r_phonenum == true && r_housenum == true) {
				
				 if ( m.length == 10 && c.length == 4 ) {
					 
					 if(p.length >= 10 && p.length <= 12) {
						if(confirm("Confirm this profile data?"))
							return true;
						else
							return false;
					}
					else {
						alert("PHONE NUMBER should be between 10-12 length!");
						$('#studphonenum').css("background-color","#ffb3b3");
						return false;
					}
				 }
				 else {
					alert("MATRIC NUMBER should be in 10 length AND\nCOLLEGE HOUSE NUMBER should be in 4 length!");
					$('#matricnum').css("background-color","#ffb3b3");
					$('#collegehousenum').css("background-color","#ffb3b3");
					return false;
				 }
			}
			else {
				alert("Insert using the right format!\n\nMATRIC NUMBER, PHONE NUMBER and COLLEGE HOUSE NUMBER should be in numeric values!");
				$('#matricnum').css("background-color","#ffb3b3");
				$('#studphonenum').css("background-color","#ffb3b3");
				$('#collegehousenum').css("background-color","#ffb3b3");
				return false;
			}
		})
		
		$('button[name="UpdatePs"]').click(function()
		{	
			var sp = $("#studpassword").val();
			var csp = $("#confirmstudpassword").val();
			
			if (sp != csp) {
				alert("Confirm new password not same with new password!");
				$('#confirmstudpassword').css("background-color","#ffb3b3");
				return false;
			}
			else
				if(confirm("Confirm this password data?"))
					 return true;
				 else
					 return false;
		})
	});
	
	</script>

</head>
<body>
<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	

	<div style="text-align:center"><span style="color:white ; "><b>STUDENT</b></span></div>
	
	

	 <img src="courier_logo.png" style="width:50%">
  
	<h2>List of Menus</h2>
	<a id="menu" href="ST_B_dashboard.jsp"> Dashboard</a>
	<a id="menu" href="UserController?action=studentprofile" class="active"> Profile</a>
	<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
	<a id="menu" href="OrderController?action=studorderlist"> View Shipment</a>
	<a id="menu" href="UserController?action=logout"> Logout</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;color: #ffffff;">Welcome,<br><%=firstname %> | <%=role %></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;"> Edit Profile Information </h1>
	
	<!--Instruction-->
	<div class="instruction">
		<h3>Update the latest profile information</h3>
	</div>
	
	<hr class="hrStyle"><br>
	<!--Body-->
	
		<!-- PROFILE INFORMATION -->
		<form id="form" action="UserController?action=updateSprofile" method="POST">	
		
			<div class="card">
				<h3 style="text-align:center;"><br>Profile information</h3>
				<div class="container" style="background:white;">
					<table style="text-align:center-right;">
							
						<tr>
			                <td>
								<span class="border">Matric Number  </span>
							</td>
			                <td>
								<input type="text" id="matricnum" name="matricnum"  size="10" value="<c:out value="${stud.matricnum}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" disabled>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">First Name</span>
							</td>
			                <td>
								<input type="text" id="firstname" name="firstname" size="100" value="<c:out value="${stud.firstname}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">Last Name</span>
							</td>
			                <td>
								<input type="text" id="lastname" name="lastname"  size="100"  value="<c:out value="${stud.lastname}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">Gender </span>
							</td>
							<td>
								<select name="studgender" style="font-size:17px; padding-right: 11px; padding-left: 11px;">
									<c:if test="${fn:containsIgnoreCase(stud.studgender, 'F')}">
										<option value='M'>Male</option>
										<option value='F' selected>Female</option>
							      	</c:if>
							      	<c:if test = "${fn:containsIgnoreCase(stud.studgender, 'M')}">
							         	<option value='M' selected>Male</option>
										<option value='F'>Female</option>
							      	</c:if>
								</select>
							</td>
						</tr>
						
						<tr>
			                <td>
								<span class="border">Phone Number  </span>
							</td>
			                <td>
								<input type="text" id="studphonenum" name="studphonenum"  size="12" value="<c:out value="${stud.studphonenum}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">Email  </span>
							</td>
			                <td>
								<input type="email" id="email" name="studemail"  size="255" value="<c:out value="${stud.studemail}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						
						
						<tr>
							<td>
								<span class="border">College House Number </span>
							</td>
			                <td>
								<input type="text" id="collegehousenum" name="collegehousenum" size="4" value="<c:out value="${stud.collegehousenum}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">College Block </span>
							</td>
							<td>
								<select name="collegeblock" style="font-size:17px; padding-right: 11px; padding-left: 11px;">
									<option value="tsla" ${stud.collegeblock == 'tsla' ? 'selected="selected"': ''}>Tun Sri Lanang A (tsla)</option>
									<option value="tslb" ${stud.collegeblock == 'tslb' ? 'selected="selected"': ''}>Tun Sri Lanang B (tslb)</option>
									<option value="tga" ${stud.collegeblock == 'tga' ? 'selected="selected"': ''}>Tun Gemala A (tga)</option>
									<option value="tgb" ${stud.collegeblock == 'tgb' ? 'selected="selected"': ''}>Tun Gemala B (tgb)</option>
									<option value="tgc" ${stud.collegeblock == 'tgc' ? 'selected="selected"': ''}>Tun Gemala C (tgc)</option>
								</select>
							</td>
						</tr>
						
						<tr>
							<td></td>
							<td><button type="submit" id="update" name="Update" title="Button to update data into database" style="cursor:pointer;">Update Profile</button></td>
						</tr>
					
					</table> 
			 </div>
		 </div>
	</form>
	
	<br><br>
	
	<!-- ////////////////////////////////////////////////////////// -->
	<!-- PASSWORD -->
	<form id="form" action="UserController?action=updateSprofile" method="POST">	
		
		<div class="card">
			<h3 style="text-align:center;"><br>Edit New Password</h3>
			<div class="container" style="background:white;">
				<table style="text-align:center-right;">					
					<tr>
						<td><span class="border">New Password</span></td>
		                <td><input type="password" id="studpassword" name="studpassword"  size="50" maxlength = "12"  style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required></td>
					</tr>
					
					<tr>
						<td><span class="border">Confirm New Password</span></td>
		                <td><input type="password" id="confirmstudpassword" name="confirmstudpassword"  size="50" maxlength = "12"  style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required></td>
					</tr>
					
					<tr>
						<td></td>
						<td><button type="submit" id="update" name="UpdatePs" title="Button to update password into database" style="cursor:pointer;">Update Password</button></td>
					</tr>
				
				</table> 
		 </div>
	 </div>
</form>
	
</div><br>


<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>

</body>
</html>