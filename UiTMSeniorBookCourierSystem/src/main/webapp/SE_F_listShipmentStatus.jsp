<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("senior") != true)	  
      response.sendRedirect("SE_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>List of Shipment</title>
	
	<link rel="stylesheet" href="Senior/sidebar_se.css">
	<link rel="stylesheet" href="Senior/header_se.css">
	<link rel="stylesheet" href="Senior/body_se.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		.viewButton { background-color: #365cc7; }
		.viewButton:hover {background-color: #26428c; color:white;}
		.acceptButton {background-color: #770077; color:white;}
		.acceptButton:hover {background-color: #5b005b; color:white;}
	</style>
	<% 
		//Display alert message 
		String alert = (String) request.getAttribute("getAlert");
	
		if(alert != null) 
		{
			if(alert.equalsIgnoreCase("Update to prepared"))
			{
				%>
				<script type="text/javascript">
					alert("Shipment status have been successfully updated to prepared");
				</script>
				<%
			}
			else if(alert.equalsIgnoreCase("Update to delivered")) 
			{
				%>
				<script type="text/javascript">
					alert("Shipment status have been successfully updated to delivered");
				</script>
				<%
			}
		}
	%>
	</head>
	<body>
		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		  
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>SENIOR</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="SE_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=empprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=listshipmentrequest"> Shipment Request</a>
			<a id="menu" href="OrderController?action=listshipment" class="active"> List of Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
		
		<!-- HEADER -->
		<div id="header">
		
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>
			
		</div>
		
		<!-- CONTENT -->
		<div id="body">
		
			<!--Title -->
			<h1 style="color:black;">List of Shipment</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>List of your shipment that has been accepted, prepared and delivered.</h3>
				<h3>View your parcel evidence on each shipment.</h3>
			</div>
			
			<hr class="hrStyle"><br>
			
			<!--Body-->
			<div class="shipReqList" align="left">
				<table border="1">
					<tr style="background-color: #d7edbb;">
						<th>ORDER</th>
						<th>ADVERTISEMENT</th>
						<th>STUDENT</th>
						<th>COURIER</th>
						<th>STATUS</th>
						<th>VIEW EVIDENCE</th>
					</tr>
					<!-- Display list of shipment (accepted, prepared, delivered) -->
					<c:forEach items="${orders}" var="order">
						<tr>
						<!-- ORDER -->
							<td>
								<span>ID:</span> <c:out value="${order.orderid}"/><br>
								<span>Order Date:</span><br><c:out value="${order.orderdate}"/><br><br>
								<span>Total Price:</span><br>RM <c:out value="${order.totalprice}"/><br>
							</td>
							<td style="width: 20%;">
								<c:out value="${order.ads.adstitle}"/>
							</td>
							<td>
								<span><c:out value="${order.students.firstname} ${order.students.lastname}"/></span><br><br>
								<span>Gender:</span> <c:out value="${order.students.studgender}"/><br>
								<span>Phone Number:</span><br><c:out value="${order.students.studphonenum}"/><br>
								<span>Email:</span><br><c:out value="${order.students.studemail}"/><br>
								<span>College:</span><br><c:out value="${order.students.collegehousenum}"/> <c:out value="${order.students.collegeblock}"/><br>
							</td>
							<td>
								<span><c:out value="${order.typeofdelivery}"/></span><br><br>
								<span>Date:</span><br><c:out value="${order.mddate}"/><br>
								<span>Time:</span> <c:out value="${order.mdtime}"/><br>
								<span>Place:</span><br><c:out value="${order.mdplace}"/>
							</td>
							<td>
						    	<c:if test = "${order.status eq 'delivered'}">
									<c:out value="${order.status}" />
						      	</c:if>
						      	<c:if test = "${order.status ne 'delivered'}">
						         	<a href="OrderController?action=updatestatus&orderid=<c:out value="${order.orderid}"/>&status=<c:out value="${order.status}"/>">
		              					<button class="acceptButton" name="accept" title="Button to change status of this order" onclick="return confirm('Are you sure you want to change status of this order?');"><c:out value="${order.status}"/></button>
		            				</a>
						      	</c:if>
							</td>
							<!-- VIEW EVIDENCE -->
							<td>
								<a href="OrderController?action=viewevidence&orderid=<c:out value="${order.orderid}"/>">
			              			<button class="viewButton" name="accept" title="Button to view evidence">View<br>Evidence</button>
			            		</a>
							</td>
							
						</tr>
					</c:forEach>
					<!------------------------------------------------------------>
					<tr style="background-color: #d7edbb;">
						<th colspan="6"><b>Total List of Shipment(s) : <c:out value="${fn:length(orders)}" /></th>
					</tr>
				</table>
			</div>
		</div><br>
		
		<script>
			//Script to open dan close side menu 
			function openNav() {
			  document.getElementById("mySidenav").style.width = "250px";
			  document.getElementById("header").style.marginLeft = "250px";
			  document.getElementById("body").style.marginLeft="250px";
			}
			
			function closeNav() {
			  document.getElementById("mySidenav").style.width = "0";
			  document.getElementById("header").style.marginLeft= "0";
			  document.getElementById("body").style.marginLeft="0";
			}
		</script>
	</body>
</html>