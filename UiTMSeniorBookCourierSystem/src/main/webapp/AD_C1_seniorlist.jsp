<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("admin") != true)	  
      response.sendRedirect("AD_A_index.jsp");
%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> <%-- take length from the list --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Senior List</title>
<link rel="stylesheet" href="Admin/sidebar_ad.css">
	<link rel="stylesheet" href="Admin/header_ad.css">
	<link rel="stylesheet" href="Admin/body_ad.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		
		/*List*/ 
		table
		{
			border-collapse: separate; border-spacing: 0; text-align: center;
			width:100%;
			border: 1px solid #cccccc;
		}
		
		th
		{
			background-color: #2c3338; color: white;
			padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
		}
		
		td
		{
			border-top: 1px solid #cccccc;
			padding-top: 25px; padding-right: 25px; padding-bottom: 25px; padding-left: 25px;
		}
		.button {
		background-color: #BF40BF;
		border: none;
		color: white;
		padding: 14px 30px;
		text-align: center;
		cursor: pointer;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
		}
	</style>
</head>
<body>
<div id="mySidenav" class="sidenav">
  
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	
	<div style="text-align:center"><span style="color:white ; "><b>ADMIN</b></span></div>


	<img src="courier_logo.png" style="width:50%">
  
	<h2>List of Menus</h2>
	<a id="menu" href="OrderController?action=statistics"> Dashboard</a>
	<a id="menu" href="UserController?action=seniorlist" class="active"> Senior List</a>
	<a id="menu" href="ChargeController"> Update Delivery Charge</a>
	<a id="menu" href="UserController?action=empprofile"> Profile</a>
	<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
	<a id="menu" href="UserController?action=logout"> Logout</a>

</div>
<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>

</div>

<!-- CONTENT -->
	<div id="body">

		<!--Title -->
		<h1 style="color: black;">Senior List</h1>

		<!--Instruction-->
		<div class="instruction">
			<h3>List of all senior</h3>
		</div>

		<hr class="hrStyle">

		<div align="right">
			<a href="AD_C3_senioradd.jsp" class="button">Add Senior</a>
		</div><br>
		
		<%
			//Display alert message 
			String alert = (String) request.getAttribute("result");
			
			if(alert != null) 
			{
				if(alert.equalsIgnoreCase("Successfully registered")) //after add advertisement
				{
					%>
					<script type="text/javascript">
						alert("Senior successfully registered");
					</script>
					<%
				}
				if(alert.equalsIgnoreCase("Successfully deleted")) //after add advertisement
				{
					%>
					<script type="text/javascript">
						alert("Senior successfully deleted");
					</script>
					<%
				}
			}
		%>
		
		<table border="1">
			<tr>
				<th>EMPLOYEE<br>ID</th>
				<th>NAME</th>
				<th>GENDER</th>
				<th>ROLE</th>
				<th>ACTION</th>
			</tr>
			<c:forEach items="${seniors}" var="senior">
				<tr>
					<td><a href="UserController?action=seniorview&empid=<c:out value="${senior.empid}"/>" style="padding: 10px;"><c:out value="${senior.empid}" /></a></td>
					<td><c:out value="${senior.name}" /></td> 
					<td><c:out value="${senior.empgender}" /></td>
					<td><c:out value="${senior.role}" /></td>
					<td>
						<input type="hidden" name="seniorname" value="<c:out value="${senior.name}" />">
						<a href="UserController?action=seniordelete&empid=<c:out value="${senior.empid}"/>">
							<button name="delete" title="Button to delete this advertisement" onclick="return confirm('Are you sure you want to delete this senior?');">Delete</button>
						</a>
					</td>
				</tr>
			</c:forEach>
			<tr>
			
				<td colspan="9" style="text-align: left;">Total Senior Student : <c:out value="${fn:length(seniors)}" /></td>
			</tr>
		</table>
	</div>
	<br>

<script>
//Script to open dan close side menu 
function openNav() {
  	document.getElementById("mySidenav").style.width = "250px";
  	document.getElementById("header").style.marginLeft = "250px";
  	document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
	document.getElementById("mySidenav").style.width = "0";
  	document.getElementById("header").style.marginLeft= "0";
  	document.getElementById("body").style.marginLeft="0";
}
</script><script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>

<script>

$(document).ready(function()
{
	$('button[type="submit"]').click(function()
	{	
		var r_phonenum = $.isNumeric($("#empphonenum").val());
		var p = $("#empphonenum").val();
		
		if (r_phonenum == true) {
			
			if(p.length >= 10 && p.length <= 12) {
				if(confirm("Confirm this data?"))
					return true;
				else
					return false;
			}
			else {
				alert("PHONE NUMBER should be between 10-12 length!");
				$('#empphonenum').css("background-color","#ffb3b3");
				return false;
			}
		}
		else {
			alert("Insert using the right format!\n\nPHONE NUMBER should be in numberic values!");
			$('#empphonenum').css("background-color","#ffb3b3");
			return false;
		}
	})
});

</script>
</body>
</html>