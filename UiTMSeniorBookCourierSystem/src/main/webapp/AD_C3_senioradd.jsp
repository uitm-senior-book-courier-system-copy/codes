<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("admin") != true)	  
      response.sendRedirect("AD_A_index.jsp");
%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add Senior</title>
	<link rel="stylesheet" href="Admin/sidebar_ad.css">
	<link rel="stylesheet" href="Admin/header_ad.css">
	<link rel="stylesheet" href="Admin/body_ad.css">
	
	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	
<style>
		body {font-family: "Lato", sans-serif; background: white;}
		.tdtattr {padding-left: 200px; width:38%;}
		.tdinput {padding-left: 20px;}
		
		input[type=email] {
		  width: 20%; padding: 12px 20px; margin: 8px 0; display: inline-block;
		  border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box;
		}
</style>

</head>
<body>

	
<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	
	<div style="text-align:center"><span style="color:white ; "><b>ADMIN</b></span></div>


	<img src="courier_logo.png" style="width:50%">
  
	<h2>List of Menus</h2>
	<a id="menu" href="OrderController?action=statistics"> Dashboard</a>
	<a id="menu" href="UserController?action=seniorlist" class="active"> Senior List</a>
	<a id="menu" href="ChargeController"> Update Delivery Charge</a>
	<a id="menu" href="UserController?action=empprofile"> Profile</a>
	<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
	<a id="menu" href="UserController?action=logout"> Logout</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>

</div>

<!-- CONTENT -->
<div id="body">


	<!--Title -->
	<h1 style="color:black;">Add Senior</h1>
	
	<!--Instruction-->
	<div class="instruction">
		<h3>Enter senior details</h3>
	</div>
	
	<hr class="hrStyle">
	
	<!--Body-->
	
	<% String result = (String) request.getAttribute("result"); %>

	<%if (result != null) { %>
		<h4><%=result%></h4>
	<%}%>
	
		<form action="UserController?action=senioradd" method="post">
			<table style="text-align: left;">
				<tr id="hidden">
					<td class="tdtattr"><span class="border">PASSWORD </span></td>
					<td class="tdinput"><input type="password" name="emppassword"
						placeholder="Enter Password" size="12" 
						style="resize: none; width: 60%; font-size: 17px; font-family: 'Lato', sans-serif;" required/>
					</td>
				</tr>
				<tr id="hidden">
					<td class="tdtattr"><span class="border">USERNAME </span></td>
					<td class="tdinput"><input type="text" name="username"
						placeholder="Enter Username" size="50" 
						style="resize: none; width: 60%; font-size: 17px; font-family: 'Lato', sans-serif;" required/>
					</td>
				</tr>
				<tr id="hidden">
					<td class="tdtattr"><span class="border">NAME </span></td>
					<td class="tdinput"><input type="text" name="name" 
						placeholder="Enter Name" size="200" required
						style="resize: none; width: 60%; font-size: 17px; font-family: 'Lato', sans-serif;"/>
					</td>
				</tr>
				<tr id="hidden">
					<td class="tdtattr"><span class="border">GENDER </span></td>
					<td class="tdinput">
						<select id="types" name="empgender" style="resize: none; width: 60%; font-size: 17px; font-family: 'Lato', sans-serif;">
							<option value="M">MALE (M)</option>
							<option value="F">FEMALE (F)</option>
					 	</select>
					</td>
				</tr>
				<tr id="hidden">
					<td class="tdtattr"><span class="border">PHONE NUMBER </span></td>
					<td class="tdinput"><input type="text" name="empphonenum"
						placeholder="Enter Phone Number" maxlength = "12"  size="12" required id="empphonenum"
						style="resize: none; width: 60%; font-size: 17px; font-family: 'Lato', sans-serif;"/>
					</td>
				</tr>
				<tr id="hidden">
					<td class="tdtattr"><span class="border">EMAIL ADDRESS </span></td>
					<td class="tdinput"><input type="email" name="empemail"
						placeholder="Enter Email Address" size="500" required
						style="resize: none; width: 60%; font-size: 17px; font-family: 'Lato', sans-serif;"/>
					</td>
				</tr>
				<tr id="hidden">
					<td class="tdtattr"><span class="border">ROLE </span></td>
					<td class="tdinput">
						<select id="types" name="role" style="resize: none; width: 60%; font-size: 17px; font-family: 'Lato', sans-serif;">
							<option value="senior">SENIOR</option>
					 	</select>
					 </td>
				</tr>
				<tr id="hidden">
					<td></td>
					<td class="tdinput"><button type="submit" id="add" value="Submit"
							title="Add senior into database">Add Senior</button>
						<button type="reset" id="Cancel" name="Reset" onclick="window.location.href='UserController?action=seniorlist&'"
							style="margin-left: 10px;" title="Button to go back">Cancel</button>
					</td>
				</tr>

			</table>
		</form>
	</div>
<br>


<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}

$(document).ready(function()
{
	$('button[type="submit"]').click(function()
	{	
		var r_phonenum = $.isNumeric($("#empphonenum").val());
		var p = $("#empphonenum").val();
		
		if (r_phonenum == true) {
			
			if(p.length >= 10 && p.length <= 12) {
				if(confirm("Confirm this data?"))
					return true;
				else
					return false;
			}
			else {
				alert("PHONE NUMBER should be between 10-12 length!");
				$('#empphonenum').css("background-color","#ffb3b3");
				return false;
			}
		}
		else {
			alert("Insert using the right format!\n\nPHONE NUMBER should be in numberic values!");
			$('#empphonenum').css("background-color","#ffb3b3");
			return false;
		}
	})
});
</script>
</body>
</html>