<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("senior") != true)	  
      response.sendRedirect("SE_A_index.jsp");
  %>
<!DOCTYPE html>
<html>
<head>
		<link rel="stylesheet" href="Senior/sidebar_se.css">
		<link rel="stylesheet" href="Senior/header_se.css">
		<link rel="stylesheet" href="Senior/body_se.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}	
	</style>
	<meta charset="ISO-8859-1">
	<title>Add Advertisement</title>
	</head>
	<body>
	
		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		  
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>SENIOR</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="SE_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=empprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist" class="active"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=listshipmentrequest"> Shipment Request</a>
			<a id="menu" href="OrderController?action=listshipment"> List of Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
		
		<!-- HEADER -->
		<div id="header">
		
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>
		
		</div>
		
		<!-- CONTENT -->
		<div id="body">
		
			<!--Title -->
			<h1 style="color:black;">Add Advertisement</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>Enter details of the new advertisement</h3>
			</div>
			
			<hr class="hrStyle"><br>
			
			<!--Body-->
			
			<!-----------------------ADD ADVERTISEMENT FORM ----------------------------------->
			<form action="AdvertisementController?action=addads" method="post" enctype="multipart/form-data">
				
				<table id="adsTable" cellpadding="10">
					<tr>
						<td width="13%">
							<label for="adstitle" id="fieldLabel">Title <span class="required">*</span></label>
						</td>
						<td>
							<input type="text" id="adsText" name="adstitle" maxlength="50" placeholder="Enter title here..." required/>
						</td>
					</tr>
					
					<tr>
						<td>
							<label for="adsdesc" id="fieldLabel">Description <span class="required">*</span></label>
						</td>
						<td>
							<textarea id="adsText" name="adsdesc" rows="4" cols="48" placeholder="Enter description here..." maxlength="200" required></textarea>
						</td>
					</tr>
					
					<tr>
						<td>
							<label for="price" id="fieldLabel">Price (RM) <span class="required">*</span> </label>
						</td>
						<td>
							<input type="number" id="adsNumber" name="price" style="width:20%" placeholder="0.00" step="0.10" min="0.00" max="9999.99" required>
						</td>
					</tr>
					
					<tr>
						<td>
							<label for="images" id="fieldLabel">Image</label>
						</td>
						<td>
							<input type="file" class="file" id="images" name="adspicture" accept="image/png, image/jpeg">
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<br>
							<input type="submit" id="add" name="Add" style="margin-left:10px;" title="Button to add this advertisement" onclick="return confirm('Add this advertisement?')" value="Add" />
					
							<a href = "AdvertisementController?action=adslist">
								<input type="button" id="Cancel" name="Cancel" style="margin-left:10px;" title="Button to go back to List of Advertisement page" value="Cancel" />
							</a>
						</td>
					</tr>
				</table>
				
			</form>	
			<!--------------------------------------------------------------------------------->
			
		</div><br>
		
		<script>
			//Script to open dan close side menu 
			function openNav() {
			  document.getElementById("mySidenav").style.width = "250px";
			  document.getElementById("header").style.marginLeft = "250px";
			  document.getElementById("body").style.marginLeft="250px";
			}
			
			function closeNav() {
			  document.getElementById("mySidenav").style.width = "0";
			  document.getElementById("header").style.marginLeft= "0";
			  document.getElementById("body").style.marginLeft="0";
			}	
		</script>
		
	</body>
</html>