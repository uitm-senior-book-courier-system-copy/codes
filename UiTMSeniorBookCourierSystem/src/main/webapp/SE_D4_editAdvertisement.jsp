<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("senior") != true)	  
      response.sendRedirect("SE_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="Senior/sidebar_se.css">
		<link rel="stylesheet" href="Senior/header_se.css">
		<link rel="stylesheet" href="Senior/body_se.css">
		
		<style>
			body {font-family: "Lato", sans-serif; background: white;}	
		</style>
		<meta charset="ISO-8859-1">
		<title>Edit Advertisement</title>
	</head>
	<body>
	
		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		  
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>SENIOR</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="SE_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=empprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist" class="active"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=listshipmentrequest"> Shipment Request</a>
			<a id="menu" href="OrderController?action=listshipment"> List of Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
		
		<!-- HEADER -->
		<div id="header">
		
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		   <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>
		
		</div>
		
		<!-- CONTENT -->
		<div id="body">
		
			<!--Title -->
			<h1 style="color:black;">Edit Advertisement</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>You can update details of the advertisement</h3>
			</div>
			
			<hr class="hrStyle">
			
			<!--Body-->
			
			<h3 style="text-align:center;"><br>Update Advertisement Information</h3>
			<!-----------------------EDIT ADVERTISEMENT FORM ----------------------------------->
			<form action="AdvertisementController?action=editads&adsid=<c:out value="${ad.adsid}"/>" method="POST">
				
				<table id="adsTable" cellpadding="10">		
					<tr>
						<td width="13%">
							<label for="adstitle" id="fieldLabel">Title </label>
						</td>
						<td>
							<input type="text" id="adsText" name="adstitle" maxlength="50" value="<c:out value="${ad.adstitle}"/>" placeholder="<c:out value="${ad.adstitle}"/>" required/>
						</td>
					</tr>
					
					<tr>
						<td>
							<label for="desc" id="fieldLabel">Description </label>
						</td>
						<td>
							<textarea id="adsText" name="adsdesc" rows="4" cols="48" maxlength="500" placeholder="<c:out value="${ad.adsdesc}"/>" required><c:out value="${ad.adsdesc}" /></textarea>
						</td>
					</tr>
					
					<tr>
						<td>
							<label for="price" id="fieldLabel">Price (RM) </label>
						</td>
						<td>
							<input type="number" id="adsNumber" name="price" style="width:20%" placeholder="0.00" step="0.10" min="0.00" max="9999.99"
							value="<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${ad.price}"/>" required>
						</td>
					</tr>
					
					<tr>
						<td></td>
						<td>
							<br>
							<button type="submit" id="update" name="Update" onclick="return confirm('Change the advertisement?')" title="Button to update details of this advertisement">Update</button>
						</td>
					</tr>
				</table>
				
			</form>
			<!--------------------------------------------------------------------------------->
			
			<br>
			<h3 style="text-align:center;"><br>Update Advertisement Picture</h3>
			<!-----------------------EDIT ADVERTISEMENT PICTURE ----------------------------------->
			<form action="AdvertisementController?action=addadspic&adsid=<c:out value="${ad.adsid}"/>" method="post" enctype="multipart/form-data">
				
				<table id="adsTable" cellpadding="10">		
					<tr>
						<td width="13%">
							<label for="adstitle" id="fieldLabel">Picture</label>
						</td>
						<td>
							<c:if test = "${not empty ad.adspicture}">
					         	<img src="data:image/jpg;base64,${ad.adspicture}" alt="Advertisement Image" name="adspicture" id="smallImage" />
					      	</c:if>
					      	<c:if test = "${empty ad.adspicture}">
					         	<img src="No-Image-Placeholder.png" alt="Advertisement Image" name="adspicture" id="smallImage" />
					      	</c:if>
						</td>
					</tr>
					
					<tr>
						<td></td>
						<td>
							<input type="file" class="file" id="images" name="adspicture" accept="image/png, image/jpeg">
							<br> <br>
							<button type="submit"  id="update" onclick="return confirm('Change the picture?')">Update</button>
						</td>
					</tr>
				</table>
			</form>
			
			<br>
			<div align="center">
				<a href = "AdvertisementController?action=view&adsid=<c:out value="${ad.adsid}"/>">
					<input type="button" id="Cancel" name="Cancel" style="margin-left:10px;" title="Button to go back to Details of Selected Advertisement page" value="Cancel" />
				</a>
			</div>
			
		</div><br>

		<script>
			//Script to open dan close side menu 
			function openNav() {
			  document.getElementById("mySidenav").style.width = "250px";
			  document.getElementById("header").style.marginLeft = "250px";
			  document.getElementById("body").style.marginLeft="250px";
			}
			
			function closeNav() {
			  document.getElementById("mySidenav").style.width = "0";
			  document.getElementById("header").style.marginLeft= "0";
			  document.getElementById("body").style.marginLeft="0";
			}
		</script>
		
	</body>
</html>