<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int studid = (Integer) session.getAttribute("id");
  String firstname = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  String studgender = (String) session.getAttribute("studgender");
  
  if(role.equalsIgnoreCase("student") != true)	  
      response.sendRedirect("ST_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> <%-- take length from the list --%>
<!DOCTYPE html>
<html>
	<head>
		<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
		<meta charset="ISO-8859-1">
		<title>View Shipment</title>
			
			<link rel="stylesheet" href="Student/sidebar_st.css">
			<link rel="stylesheet" href="Student/header_st.css">
			<link rel="stylesheet" href="Student/body_st.css">
			
			<style>
				body {font-family: "Lato", sans-serif; background: white;}
			</style>
			
			<%
				//Display alert message 
				String result = (String) request.getAttribute("result");
				
				if(result != null && result.equalsIgnoreCase("None")) 
				{
					%>
					<script type="text/javascript">
						alert("No order found");
					</script>
					<%
				}
				if(result != null && result.equalsIgnoreCase("Add Success")) //after add advertisement
				{
					%>
					<script type="text/javascript">
						alert("Product has been ordered successfully.\n\nYou can view the orderid on 'View Shipment' menu");
					</script>
					<%
				}
			%>
	</head>
	<body>

		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		  
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>STUDENT</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="ST_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=studentprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=studorderlist" class="active"> View Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
	
		<!-- HEADER -->
		<div id="header">
	
			<h3 id="user" align="center" style="margin-left: 1.5%;">Welcome
				to UITM SENIOR BOOK COURIER SYSTEM!</h3>
			<span style="font-size: 30px; cursor: pointer" onclick="openNav()">&#9776;
			</span> <span style="float: right; color: #ffffff;">Welcome,<br><%=firstname %> | <%=role %></span>
	
		</div>
	
		<!-- CONTENT -->
		<div id="body">
	
			<!--Title -->
			<h1 style="color: black;">View Shipment</h1>
	
			<!--Instruction-->
			<div class="instruction">
				<h3>Enter order id to track your shipment</h3>
			</div>
	
			<hr class="hrStyle">
			<br>
	
			<!--Body-->
			<!-----------------------VIEW SHIPMENT FORM ----------------------------------->
			<form method="POST" action="OrderController?action=viewshipment">
				<div align="center">
					<div id="containerid">
						<div id="label">
							<b>VIEW SHIPMENT</b>
						</div>
	
						<br><br> 
						<label for="orderID">Order Id :</label> 
						<input type="text" id="orderID" name="orderid" required><br>
	
						<button type="submit" class="buttons">View My Shipment!</button>
					</div>
				</div>
			</form>
			<!--------------------------------------------------------------------------------->
			<br><br>
			<div class="listadstable">
			<table border="1">
				<tr>
					<th>Order ID</th>
					<th>Order Date</th>
					<th>Buy Advertisement</th>
					<th>Status</th>
				</tr>
				
				<!-- Display list of advertisements belong to senior -->
				<c:forEach items="${orders}" var="order">
				<tr>
					<td style="width:10%;">
						<c:out value="${order.orderid}"/>
					</td>
					<td style="width:20%;">
						<c:out value="${order.orderdate}"/>
					</td>
					<td style="width:30%;">
						<c:out value="${order.ads.adstitle}"/>
					</td>
					<td style="width:20%;">
						<c:out value="${order.status}"/>
					</td>
				</tr>
				</c:forEach>
				<!------------------------------------------------------>
				<tr>
					<th colspan="6">Total Order Made: <c:out value="${fn:length(orders)}" /></th>
				</tr>
			</table>
		</div><br>
	
		<script>
			//Script to open dan close side menu 
			function openNav() {
				document.getElementById("mySidenav").style.width = "250px";
				document.getElementById("header").style.marginLeft = "250px";
				document.getElementById("body").style.marginLeft = "250px";
			}
	
			function closeNav() {
				document.getElementById("mySidenav").style.width = "0";
				document.getElementById("header").style.marginLeft = "0";
				document.getElementById("body").style.marginLeft = "0";
			}
			
			$(document).ready(function()
			{
			    $('button[type="submit"]').click(function()
			    {  
			    	var orderid = $.isNumeric($("#orderID").val());

			   		if (orderid == true) {
			          return true;
			        }
			        else {
			          alert("Insert using the right format!\n\norder id should be in numeric values!");
			          $('#orderID').css("background-color","#ffb3b3");
			          return false;
			        }
			    })
			  });
		</script>

	</body>
</html>