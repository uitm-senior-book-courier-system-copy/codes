<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("senior") != true)	  
      response.sendRedirect("SE_A_index.jsp");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Dashboard</title>
    <link rel="stylesheet" href="Senior/sidebar_se.css">
	<link rel="stylesheet" href="Senior/header_se.css">
	<link rel="stylesheet" href="Senior/body_se.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
	</style>
</head>
<body>
<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	
	<div style="text-align:center"><span style="color:white ; "><b>SENIOR</b></span></div>

	<img src="courier_logo.png" style="width:50%">
  
	<h2>List of Menus</h2>
	<a id="menu" href="SE_B_dashboard.jsp" class="active"> Dashboard</a>
	<a id="menu" href="UserController?action=empprofile"> Profile</a>
	<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
	<a id="menu" href="OrderController?action=listshipmentrequest"> Shipment Request</a>
	<a id="menu" href="OrderController?action=listshipment"> List of Shipment</a>
	<a id="menu" href="UserController?action=logout"> Logout</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>

</div>

<!-- CONTENT -->
<div id="body">
	
	<!--Title -->
	<h1 style="color:black;">Dashboard</h1>
	
	<!--Instruction-->
	<div class="instruction">
		<h3>Welcome to the dashboard</h3>
	</div>
	
	<hr class="hrStyle"><br>
	
	<!--Body-->
	<h3>In this system, senior can:</h3>
	<ul>
		<li>Edit profile</li>
		<li>Add/edit/delete advertisement</li>
		<li>Shipment request</li>
		<li>View Shipment list</li>
		<li>Change status of shipment</li>
		<li>Update parcel evidence</li>
	</ul>
	
	
</div>
<br>


<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
</body>
</html>