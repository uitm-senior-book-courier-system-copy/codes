<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Welcome</title>

<link rel="stylesheet" href="Student/sidebar_st.css">
	<link rel="stylesheet" href="Student/header_st.css">
	<link rel="stylesheet" href="Student/body_st.css">
	
	<!-- Google Font -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Play&display=swap">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Heebo:wght@300&display=swap">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Doppio One'>

	<style>
		body { background: #332c38; color: white; font-family: 'Play', sans-serif; }
		
		img { width: 260px;}
		
		hr { border: 1px solid white; width: 550px; }
		
		/* link to the student index.php */
		a { text-decoration: none; color: #f678ff; }
		
		a:hover { color: #a5aee6; }
		
		.btnindx /* button Log in */ {
			text-decoration: none;	background-color: #0066ff;	border-radius: 4px;	border: none;	color: white;
			text-align: center;	text-decoration: none;	display: inline-block;	margin: 10px 2px;
			padding: 10px 10px;	width: 8%;	cursor: pointer;	font-family: 'Doppio One';	font-size: 15px;
		}
		
		.btnindx:hover {
			background-color: #0059b3;
		}
	</style>

</head>
<body>

	<% String result = (String) request.getAttribute("result"); %>

	<%if (result != null) { %>
		<h4><%=result%></h4>
	<%}%>

	<div align="center">
		<br><br>
		<div align="center">
			<img src="courier_logo.png" alt="Logo">
		</div>
		<br>
		<h1>UITM SENIOR BOOK COURIER SYSTEM</h1>
		<h2 style="font-family: 'Heebo', sans-serif;">Welcome and greeting!</h2>

		<button class="btnindx" onclick="window.location.href='AD_A_index.jsp';">ADMIN</button><br>
		<button class="btnindx" onclick="window.location.href='SE_A_index.jsp';">SENIOR</button><br>
		<button class="btnindx" onclick="window.location.href='ST_A_index.jsp';">STUDENT</button>
	</div>

</body>
</html>