<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("senior") != true)	  
      response.sendRedirect("SE_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> <%-- display two decimals places --%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> <%-- take length from the list --%>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="Senior/sidebar_se.css">
		<link rel="stylesheet" href="Senior/header_se.css">
		<link rel="stylesheet" href="Senior/body_se.css">
		
		<style>
			body {font-family: "Lato", sans-serif; background: white;}	
		</style>
		<meta charset="ISO-8859-1">
		<title>List of Advertisement</title>

		<%
		//Display alert message 
		String alert = (String) request.getAttribute("getAlert");
		
		if(alert != null) 
		{
			if(alert.equalsIgnoreCase("Add Success")) //after add advertisement
			{
				%>
				<script type="text/javascript">
					alert("Advertisement has been added successfully");
				</script>
				<%
			}
			else //after delete existing advertisement
			{
				%>
				<script type="text/javascript">
					alert("Advertisement has been deleted successfully");
				</script>
				<% 
			}
		}
		%>
	</head>
	<body>
	
		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>SENIOR</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="SE_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=empprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist" class="active"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=listshipmentrequest"> Shipment Request</a>
			<a id="menu" href="OrderController?action=listshipment"> List of Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
		
		<!-- HEADER -->
		<div id="header">
		
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>
		
		</div>
		
		<!-- CONTENT -->
		<div id="body">
			<!--Title -->
			<h1 style="color:black;">Your Advertisement List</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>List of advertisement that you have made</h3>
			</div>
			
			<hr class="hrStyle">
			
			<p align="right">
				<a href = "SE_D3_addAdvertisement.jsp">
					<button class="button" title="Button to go to Add Advertisement page">Add New</button>
				</a>
			</p>
			
			<!--Body-->
			<div class="listadstable">
			<table border="1">
				<tr>
					<th>ID</th>
					<th>Title</th>
					<th>Description</th>
					<th>Price (RM)</th>
					<th>Action</th>
				</tr>
				
				<!-- Display list of advertisements belong to senior -->
				<c:forEach items="${ads}" var="ad">
				<tr>
					<td style="width:10%;">
						<c:out value="${ad.adsid}"/>
					</td>
					<td style="width:20%;">
						<c:out value="${ad.adstitle}"/>
					</td>
					<td style="width:30%;">
						<c:out value="${ad.adsdesc}"/>
					</td>
					<td style="width:10%;">
						<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${ad.price}"/>
					</td>
					
					<td style="width:10%;">
						<a href="AdvertisementController?action=view&adsid=<c:out value="${ad.adsid}"/>" class="viewButton">View</a>
					</td>
				</tr>
				</c:forEach>
				<!------------------------------------------------------>
				
				<tr>
					<th colspan="6">Total Advertisement(s): <c:out value="${fn:length(ads)}" /></th>
				</tr>
			</table>
			</div>
			</div>
		<br>
		
		<script>
			//Script to open dan close side menu 
			function openNav() {
			  document.getElementById("mySidenav").style.width = "250px";
			  document.getElementById("header").style.marginLeft = "250px";
			  document.getElementById("body").style.marginLeft="250px";
			}
			
			function closeNav() {
			  document.getElementById("mySidenav").style.width = "0";
			  document.getElementById("header").style.marginLeft= "0";
			  document.getElementById("body").style.marginLeft="0";
			}
		</script>
		
	</body>
</html>