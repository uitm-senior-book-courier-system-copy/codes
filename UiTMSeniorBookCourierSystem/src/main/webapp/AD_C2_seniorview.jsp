<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("admin") != true)	  
      response.sendRedirect("AD_A_index.jsp");
%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>View Senior</title>
	<link rel="stylesheet" href="Admin/sidebar_ad.css">
	<link rel="stylesheet" href="Admin/header_ad.css">
	<link rel="stylesheet" href="Admin/body_ad.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		
		/*List*/ 
		table
		{
			width: 100%;
		}
		
		th
		{
			width:15%;
			background-color: #2c3338; color: white;
			padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
		}
		
		td
		{
			padding-top: 5px; padding-right: 5px; padding-bottom: 5px; padding-left: 15px;
		}
	</style>
</head>
<body>

<div id="mySidenav" class="sidenav">
  
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	
	<div style="text-align:center"><span style="color:white ; "><b>ADMIN</b></span></div>


	<img src="courier_logo.png" style="width:50%">
  
	<h2>List of Menus</h2>
	<a id="menu" href="OrderController?action=statistics"> Dashboard</a>
	<a id="menu" href="UserController?action=seniorlist" class="active"> Senior List</a>
	<a id="menu" href="ChargeController"> Update Delivery Charge</a>
	<a id="menu" href="UserController?action=empprofile"> Profile</a>
	<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
	<a id="menu" href="UserController?action=logout"> Logout</a>

</div>
<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>

</div>

<!-- CONTENT -->
	<div id="body">

		<h1 style="color: black;">Senior List</h1>

		<!--Instruction-->
		<div class="instruction">
			<h3>Details of senior <c:out value="${senior.name}"/></h3>
		</div>

		<hr class="hrStyle">
		
	<table border="0">
			<tr>
				<th>Employee Id</th>
				<td><c:out value="${senior.empid}"/></td>
			</tr>
			<tr>
				<th>Name</th>
				<td><c:out value="${senior.name}"/></td>
			</tr>
			<tr>
				<th>Gender</th>
				<td><c:out value="${senior.empgender}"/></td>
			</tr>
			<tr>
				<th>Phone Number</th>
				<td><c:out value="${senior.empphonenum}"/></td>
			</tr>
			<tr>
				<th>Email</th>
				<td><c:out value="${senior.empemail}"/></td>
			</tr>
			<tr>
				<th>Role</th>
				<td><c:out value="${senior.role}"/><br></td>
			</tr>
	</table>
	
	<br>
	<button type="reset" id="Cancel" name="Reset" onclick="window.location.href='UserController?action=seniorlist&'"
			style="margin-left: 10px;" title="Button to go back">Back</button>
	
	</div>
</body>
</html>