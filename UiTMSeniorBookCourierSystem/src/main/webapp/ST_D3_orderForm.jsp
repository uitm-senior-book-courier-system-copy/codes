<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int studid = (Integer) session.getAttribute("id");
  String firstname = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  String studgender = (String) session.getAttribute("studgender");
  
  if(role.equalsIgnoreCase("student") != true)	  
      response.sendRedirect("ST_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="Student/sidebar_st.css">
		<link rel="stylesheet" href="Student/header_st.css">
		<link rel="stylesheet" href="Student/body_st.css">
		
		<!-- jQuery -->
		<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
		
		<style>
			body {font-family: "Lato", sans-serif; background: white;}	
			#update:hover{background-color: #365cc7; cursor: pointer;}
		</style>
		<meta charset="ISO-8859-1">
		<title>Make Order</title>
	</head>
	<body>
	
		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		  
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>STUDENT</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="ST_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=studentprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist" class="active"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=studorderlist"> View Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
		
		<!-- HEADER -->
		<div id="header">
		
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=firstname %> | <%=role %></span>
		
		</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">Order Form</h1>
	
	<!--Instruction-->
	<div class="instruction">
		<h3>Please fill in the order form below</h3>
	</div>
	
	<hr class="hrStyle"><br>
				
	<!--Body-->
		<form action="OrderController?action=addOrder&" method="POST">
			
			<h3><b>PRODUCT INFORMATION</b></h3>
			<table class="productInfo" >
				<tr>
					<td rowspan="4">
						<div align="center" id="adsImgBorder">
							<c:if test = "${not empty ad.adspicture}">
								<img src="data:image/jpg;base64,${ad.adspicture}" id="smallImage" />
					      	</c:if>
					      	<c:if test = "${empty ad.adspicture}">
					         	<img src="No-Image-Placeholder.png" id="smallImage" />
					      	</c:if>
						</div>
					</td>
					<td id="fieldProduct" width="12%" style="background:#e6ccff;">
						Title: <input type="hidden" name="adsid" value="${ad.adsid}">
					</td>
					<td>
						<c:set var="adstitle" value="${ad.adstitle}" />
						<c:out value="${adstitle}"/>
					</td>
				</tr>
				
				<tr>
					<td id="fieldProduct">
						Sold by:
					</td>
					<td>
						<c:out value="${ad.employees.name}" /> (<c:out value="${ad.employees.empgender}" />)
					</td>
				</tr>
				
				<tr>
					<td id="fieldProduct">
						Price
					</td>
					<td>
						<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${ad.price}"/>
					</td>	
				</tr>
				
				<tr>
					<td id="fieldProduct">
						Estimated Total Price 
						<input type="hidden" name="totalprice" value="${ad.price + charge.chargeonorder + 1}">
						<input type="hidden" name="rm1chargeOn" value="${charge.chargeontype}">
					</td>
					<td>
						<b>Charge On Weight:</b> RM <c:out value="${charge.chargeonorder}" /><br>
						<b>RM 1 Charge On:</b> <c:out value="${charge.chargeontype}" /><br><br>
						<b>Estimated Total Price :</b> RM <c:out value="${ad.price + charge.chargeonorder + 1}" />
					</td>
				</tr>
				
			</table>
			
			<br><br>
			
			
			<h3><b>ORDER FORM</b></h3>
			
			<div class="border">
				<table class="orderForm" >	
				
					<c:set var="empgender" value="${ad.employees.empgender}" />
					<c:set var="studgender" value="<%=studgender %>" />
					<c:set var="date" value="<%=new java.util.Date() %>" />
					
					<tr>
						<td>
							<span class="fieldBorder">Today </span>
						</td>
						<td>
							<input type="text" name="orderdate" value="<fmt:formatDate type="date" pattern="yyyy-MM-dd" value="${date}"/>" readonly>
						</td>
					</tr>
					
					<tr>
						<td>
							<span class="fieldBorder">Type of Delivery </span>
						</td>
						<td>
							RM 1 will be charge on <c:out value="${charge.chargeontype}" /><br>
							<select id="typeofdelivery" name="typeofdelivery">
								<c:if test="${empgender ne studgender}"> <%-- not equal --%>
									<option value="meetup">Meetup</option>
								</c:if>
								<c:if test="${empgender eq studgender}">
									<option value="delivery">Delivery</option>
									<option value="meetup">Meetup</option>
								</c:if>
							</select>
						</td>
					</tr>
					
					<c:if test="${empgender eq studgender}">
					<tr>
						<td>
							<span class="fieldBorder">Location (Delivery)</span>
						</td>
						<td>
							If type of delivery is meetup, this will be ignored
							<select id="mdplace" name="mdplaceD">
					      		<option value="kolej">Kolej </option>
					      	</select>
						</td>
					</tr>
					</c:if>
					
					<tr>
						<td>
							<span class="fieldBorder">Location (Meetup)</span>
						</td>
						<td>
							If type of delivery is delivery, this will be ignored
							<select id="mdplace" name="mdplaceM">
					      		<option value="medan selera">Medan Selera </option>
								<option value="depan pb kolej">Depan PB Kolej </option>
							</select>
						</td>
					</tr>
					
					<tr>
						<td width="24%">
							<span class="fieldBorder"><span id="tod"></span> Date</span>
						</td>
						<td>
							<input type="date" id="date" name="mddate" width="100%" required>
						</td>
					</tr>
					
					<tr>
						<td>
							<span class="fieldBorder">Time</span>
						</td>
						<td>
							<select id="time" name="mdtime">
								<option value="12pm">12 pm </option>
								<option value="6pm">6 pm </option>
								<option value="9pm">9 pm </option>
							</select>
						</td>
					</tr>

					<tr>
						<td></td>
						<td>
							<br>
							<button type="submit" id="update" name="AddOrder" title="Button to checkout the order" style="cursor:pointer;">Checkout</button>
							<button type="button" name="Cancel" id="Cancel" class="Cancel" onclick="window.location.href='AdvertisementController?action=adslist';">Back</button>
							<!-- <button type="submit" id="Cancel" name="Cancel"  title="Button to go back to list">Cancel</button> -->
						</td>
					</tr>
				</table>
			</div>
		</form>
</div>
<br>


<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}

var today = new Date().toISOString().split('T')[0];
document.getElementsByName("mddate")[0].setAttribute('min', today);

</script>

<script>
$(document).ready(function()
{
	$('button[type="submit"]').click(function()
	{	
		var typeofdelivery = $("#typeofdelivery").val();
		var mdplaceD = $("select[name='mdplaceD']").val();
		var mdplaceM = $("select[name='mdplaceM']").val();
		var mddate = $('#date').val();
		var mdtime = $("select[name='mdtime']").val();
		var estimatedtotalprice = parseFloat($("input[name='totalprice']").val());
		var rm1chargeon = $("input[name='rm1chargeOn']").val();
		
		if (typeofdelivery == 'delivery') {
			 
			var totalprice = 0;
			
			if(rm1chargeon == 'delivery') //user choose delivery & the RM1 charge is on delivery
				totalprice = estimatedtotalprice;
			else
				totalprice = estimatedtotalprice-1;
			
			if(confirm("Estimated total price: RM "+estimatedtotalprice+"\nTotal price: RM "+totalprice+"\n\nProceed?"))
				 return true;
			 else
				 return false;
		}
		else { //meetup
			 
			 var totalprice = 0;
				
				if(rm1chargeon == 'meetup') //user choose meetup & the RM1 charge is on meetup
					totalprice = estimatedtotalprice;
				else
					totalprice = estimatedtotalprice-1;
			 
			 if(confirm("Estimated total price: RM "+estimatedtotalprice+"\nTotal price: RM "+totalprice+"\n\nProceed?"))
				 return true;
			 else
				 return false;
		 }
	})
});
</script>
   
</body>
</html>