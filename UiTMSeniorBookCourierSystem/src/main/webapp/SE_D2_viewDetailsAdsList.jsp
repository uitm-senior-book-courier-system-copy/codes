<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("senior") != true)	  
      response.sendRedirect("SE_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="Senior/sidebar_se.css">
		<link rel="stylesheet" href="Senior/header_se.css">
		<link rel="stylesheet" href="Senior/body_se.css">
		
		<style>
			body {font-family: "Lato", sans-serif; background: white;}	
		</style>
		<meta charset="ISO-8859-1">
		<title>Details of Selected Advertisement</title>
		<%
			//Display alert message 
			String alert = (String) request.getAttribute("getAlert");
			
			if(alert != null) 
			{
				if(alert.equalsIgnoreCase("Update Success")) //after update existing advertisement
				{
					%>
					<script type="text/javascript">
						alert("Advertisement has been updated successfully");
					</script>
					<% 
				}
				else //"UpdatePic Success"
				{
					%>
					<script type="text/javascript">
						alert("Advertisement picture has been updated successfully");
					</script>
					<% 
				}
			}
		%>
	</head>
	<body>
	
		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		  
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>SENIOR</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="SE_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=empprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist" class="active"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=listshipmentrequest"> Shipment Request</a>
			<a id="menu" href="OrderController?action=listshipment"> List of Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
		
		<!-- HEADER -->
		<div id="header">
		
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>
		
		</div>
		
		<!-- CONTENT -->
		<div id="body">
		
			<!--Title -->
			<h1 style="color:black;">Details of Selected Advertisement</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>Details of selected advertisement with picture</h3>
			</div>
			
			<hr class="hrStyle"><br>
			
			<!--Body-->
			<div class="detail-table">
				<div align="center">
				
					<!-- Display details of selected advertisement -->
					<table>
				
						<tr>
							<td rowspan="4" id="adsImgBorder">
							
								<c:if test = "${not empty ad.adspicture}">
						         	<img src="data:image/jpg;base64,${ad.adspicture}" id="bigImage" />
						      	</c:if>
						      	<c:if test = "${empty ad.adspicture}">
						         	<img src="No-Image-Placeholder.png" id="bigImage" />
						      	</c:if>
								
							</td>
							<th>TITLE</th>
							<td><c:out value="${ad.adstitle}" /></td>
							
						</tr>
						
						<tr>
							<th>DESCRIPTION</th>
							<td><c:out value="${ad.adsdesc}" /></td>
						</tr>
						
						<tr>
							<th>PRICE</th>
							<td>RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${ad.price}"/></td>
						</tr>
						
					</table>
					<!------------------------------------------------------>
				</div>
			</div>
			
			<br><br>
			<div align="center">
				<a href = "AdvertisementController?action=adslist">
					<button class="backButton" name="back" title="Button to go back to List of Advertisement">Back</button>
				</a>
				
				<a href="AdvertisementController?action=update&adsid=<c:out value="${ad.adsid}"/>">
					<button class="editButton" name="edit" title="Button to go to Edit Advertisement page">Edit</button>
				</a>
				
				<a href="AdvertisementController?action=delete&adsid=<c:out value="${ad.adsid}"/>">
					<button class="deleteButton" name="delete" title="Button to delete this advertisement" onclick="return confirm('Are you sure you want to delete this advertisement?');">Delete</button>
				</a>
			</div>
			
		</div><br>
		
		<script>
			//Script to open dan close side menu 
			function openNav() {
			  document.getElementById("mySidenav").style.width = "250px";
			  document.getElementById("header").style.marginLeft = "250px";
			  document.getElementById("body").style.marginLeft="250px";
			}
			
			function closeNav() {
			  document.getElementById("mySidenav").style.width = "0";
			  document.getElementById("header").style.marginLeft= "0";
			  document.getElementById("body").style.marginLeft="0";
			}
		</script>
		
	</body>
</html>