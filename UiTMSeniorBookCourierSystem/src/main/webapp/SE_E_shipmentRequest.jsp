<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("senior") != true)	  
      response.sendRedirect("SE_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>List of Shipment Request</title>
	
	<link rel="stylesheet" href="Senior/sidebar_se.css">
	<link rel="stylesheet" href="Senior/header_se.css">
	<link rel="stylesheet" href="Senior/body_se.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		.acceptButton {background-color: #1ec800;}
		.acceptButton:hover {background-color: #009704; color:white;}
		.rejectButton {background-color: #e60000;}
		.rejectButton:hover {background-color: #ac1a06; color:white;}
	</style>
	
	<% 
		//Display alert message 
		String alert = (String) request.getAttribute("getAlert");
	
		if(alert != null) 
		{
			if(alert.equalsIgnoreCase("Accepted")) 
			{
				%>
				<script type="text/javascript">
					alert("Order has been accepted and will display in List of Shipment menu");
				</script>
				<%
			}
			else if(alert.equalsIgnoreCase("Rejected")) //after add advertisement
			{
				%>
				<script type="text/javascript">
					alert("Order has been rejected and will display here");
				</script>
				<%
			}
		}
	%>
	</head>
	<body>
	
		<!-- SIDE NAVIGATION -->
		<div id="mySidenav" class="sidenav">
		  
			<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
			
			<div align="center">
				<span style="color:white;"><b>SENIOR</b></span>
			</div>
		
			<img src="courier_logo.png" style="width:50%">
		  
			<h2>List of Menus</h2>
			<a id="menu" href="SE_B_dashboard.jsp"> Dashboard</a>
			<a id="menu" href="UserController?action=empprofile"> Profile</a>
			<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
			<a id="menu" href="OrderController?action=listshipmentrequest" class="active"> Shipment Request</a>
			<a id="menu" href="OrderController?action=listshipment"> List of Shipment</a>
			<a id="menu" href="UserController?action=logout"> Logout</a>
		
		</div>
		
		<!-- HEADER -->
		<div id="header">
		
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>
		
		</div>
		
		<!-- CONTENT -->
		<div id="body">
		
			<!--Title -->
			<h1 style="color:black;">Shipment Request</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>List of pending shipment that you need to accept or reject.</h3>
				<h3>List of your rejected shipment.</h3>
			</div>
			
			<hr class="hrStyle"><br>
			
			<!--Body-->
			
			<!-- PENDING ORDER -->
			<span id="PRTitle"><b>PENDING ORDER</b></span>
			
			<div class="shipReqList">
			<table border="1">
				<tr>
					<th>ORDER</th>
					<th>ADVERTISEMENT</th>
					<th>STUDENT</th>
					<th>COURIER</th>
					<th>ACTION</th>
				</tr>
				
				<!-- Display list of shipment (PENDING) -->
				<c:forEach items="${orders}" var="order">
				
				<c:set var="typeofdelivery" value="${order.typeofdelivery}" /> 
				<c:set var="ucaseType" value="${fn:toUpperCase(fn:substring(typeofdelivery, 0, fn:length(typeofdelivery)))}" />
				
				<c:set var="fullname" value="${order.students.firstname} ${order.students.lastname}" /> 
				<c:set var="ucaseName" value="${fn:toUpperCase(fn:substring(fullname, 0, fn:length(fullname)))}" />
				
				<tr>
					<td>
						<span>ID:</span> <c:out value="${order.orderid}"/><br>
						<span>Order Date:</span><br><c:out value="${order.orderdate}"/><br><br>
						<span>Total Price:</span><br>RM <c:out value="${order.totalprice}"/><br>
					</td>
					<td style="width: 20%;">
						<c:out value="${order.ads.adstitle}"/>
					</td>
					<td style="width: 30%;">
						<span><c:out value="${ucaseName}"/></span><br><br>
						<span>Gender:</span> <c:out value="${order.students.studgender}"/><br>
						<span>Phone Number:</span><br><c:out value="${order.students.studphonenum}"/><br>
						<span>Email:</span><br><c:out value="${order.students.studemail}"/><br>
						<span>College:</span><br><c:out value="${order.students.collegehousenum}"/> <c:out value="${order.students.collegeblock}"/><br>
					</td>
					<td>
						<span><c:out value="${ucaseType}"/></span><br><br>
						<span>Date:</span><br><c:out value="${order.mddate}"/><br>
						<span>Time:</span> <c:out value="${order.mdtime}"/><br>
						<span>Place:</span><br><c:out value="${order.mdplace}"/>
					</td>
					<td>
						<div align="center">
							<a href="OrderController?action=accept&orderid=<c:out value="${order.orderid}"/>">
								<button class="acceptButton" name="accept" title="Button to accept this order" onclick="return confirm('Are you sure you want to accept this order?');">Accept</button>
							</a>
							<br>
							<a href="OrderController?action=reject&orderid=<c:out value="${order.orderid}"/>">
								<button class="rejectButton" name="reject" title="Button to reject this order" onclick="return confirm('Are you sure you want to reject this order?');">Reject</button>
							</a>
						</div>
					</td>
				</tr>
				</c:forEach>
				<!------------------------------------------------------>
				<tr>
					<th colspan="5">Total Pending Order(s): <c:out value="${fn:length(orders)}" /></th>
				</tr>
			</table>
			</div>
			
			<br><br>
			
			<!-- REJECTED ORDER -->
			<span id="PRTitle"><b>REJECTED ORDER</b></span>
			
			<div class="shipReqList">
			<table border="1">
				<tr>
					<th>ORDER</th>
					<th>ADVERTISEMENT</th>
					<th>STUDENT</th>
					<th>COURIER</th>
				</tr>
				<!-- Display list of shipment (REJECTED) -->
				<c:forEach items="${ordersR}" var="orderR">
					
					<c:set var="typeofdeliveryR" value="${orderR.typeofdelivery}" /> 
					<c:set var="ucaseTypeR" value="${fn:toUpperCase(fn:substring(typeofdeliveryR, 0, fn:length(typeofdeliveryR)))}" />
					
					<c:set var="fullnameR" value="${orderR.students.firstname} ${orderR.students.lastname}" /> 
					<c:set var="ucaseNameR" value="${fn:toUpperCase(fn:substring(fullnameR, 0, fn:length(fullnameR)))}" />
					
					<tr>
						<td>
							<span>ID:</span> <c:out value="${orderR.orderid}"/><br>
							<span>Order Date:</span><br><c:out value="${orderR.orderdate}"/><br><br>
							<span>Total Price:</span><br>RM <c:out value="${orderR.totalprice}"/><br>
						</td>
						<td style="width: 20%;">
							<c:out value="${orderR.ads.adstitle}"/>
						</td>
						<td style="width: 30%;">
							<span><c:out value="${ucaseNameR}"/></span><br><br>
							<span>Gender:</span> <c:out value="${orderR.students.studgender}"/><br>
							<span>Phone Number:</span><br><c:out value="${orderR.students.studphonenum}"/><br>
							<span>Email:</span><br><c:out value="${orderR.students.studemail}"/><br>
							<span>College:</span><br><c:out value="${orderR.students.collegehousenum}"/> <c:out value="${orderR.students.collegeblock}"/><br>
						</td>
						<td>
							<span><c:out value="${ucaseTypeR}"/></span><br><br>
							<span>Date:</span><br><c:out value="${orderR.mddate}"/><br>
							<span>Time:</span> <c:out value="${orderR.mdtime}"/><br>
							<span>Place:</span><br><c:out value="${orderR.mdplace}"/>
						</td>
					</tr>
				</c:forEach>
				<!------------------------------------------------------------>
				<tr>
					<th colspan="5">Total Rejected Order(s): <c:out value="${fn:length(ordersR)}" /></th>
				</tr>
			</table>
			</div>
		</div><br>
		
		<script>
			//Script to open dan close side menu 
			function openNav() {
			  document.getElementById("mySidenav").style.width = "250px";
			  document.getElementById("header").style.marginLeft = "250px";
			  document.getElementById("body").style.marginLeft="250px";
			}
			
			function closeNav() {
			  document.getElementById("mySidenav").style.width = "0";
			  document.getElementById("header").style.marginLeft= "0";
			  document.getElementById("body").style.marginLeft="0";
			}
		</script>
	
	</body>
</html>