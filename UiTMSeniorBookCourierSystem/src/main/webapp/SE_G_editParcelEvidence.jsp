<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("senior") != true)	  
      response.sendRedirect("SE_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Edit Parcel Evidence</title>
	
	<link rel="stylesheet" href="Senior/sidebar_se.css">
	<link rel="stylesheet" href="Senior/header_se.css">
	<link rel="stylesheet" href="Senior/body_se.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		
		input[type=text], textarea {
			resize:none; font-size:17px; width: 50%; padding: 12px 20px; margin: 8px 0; display: inline-block;
			border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box;
		}
	</style>
	<% 
		//Display alert message 
		String alert = (String) request.getAttribute("getAlert");
	
		if(alert != null && alert.equalsIgnoreCase("Update Success")) 
		{
			%>
			<script type="text/javascript">
				alert("Parcel evidence has been successfully edited");
			</script>
			<%	
		}
	%>
	</head>
	<body>
	
	<!-- SIDE NAVIGATION -->
	<div id="mySidenav" class="sidenav">
	  
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		
		<div align="center">
			<span style="color:white;"><b>SENIOR</b></span>
		</div>
	
		<img src="courier_logo.png" style="width:50%">
	  
		<h2>List of Menus</h2>
		<a id="menu" href="SE_B_dashboard.jsp"> Dashboard</a>
		<a id="menu" href="UserController?action=empprofile"> Profile</a>
		<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
		<a id="menu" href="OrderController?action=listshipmentrequest"> Shipment Request</a>
		<a id="menu" href="OrderController?action=listshipment" class="active"> List of Shipment</a>
		<a id="menu" href="UserController?action=logout"> Logout</a>
	
		</div>
	
		<!-- HEADER -->
		<div id="header">
	
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		   <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>
	
		</div>
	
		<!-- CONTENT -->
		<div id="body">
	
			<!--Title -->
			<h1 style="color:black;">Details of the selected Shipment</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>You can edit the parcel evidence </h3>
			</div>
			
			<hr class="hrStyle"><br>
			
			<!--Body-->
			<input type="hidden" name="adsid" value=""/>
					
			<div align="center">
			
				<div id="container2" align="center" style="height:50%">
				
				    <div id="label">EDIT PARCEL EVIDENCE</div>
				    			    
					     <br><br> <label for="orderID">Order Id :</label>
						  <input type="text" id="orderID" name="orderid" value="<c:out value="${order.orderid}"/>" readonly><br><br>
						  
						  <label for="Pname">Product Name :</label><br>
						  <textarea id="Pname" name="Pname" rows="2" cols="50" readonly><c:out value="${order.ads.adstitle}"/></textarea><br><br>
						  
						  <label for="price">Total Price: </label>
						  <input type="text" id="price" name="price" value="RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${order.totalprice}"/>" readonly><br><br>
						  
						  <label for="ship">Shipment Status:</label>
						  <input type="text" id="ship" name="ship" value="<c:out value="${order.status}"/>" readonly><br><br><br>
						  
						  <label for="evidencepic">Parcel Evidence:</label><br><br>
						  <c:choose>
						  	<c:when test="${not empty order.evidencepic}">
							    <img src="data:image/jpg;base64,${order.evidencepic}" id="smallImage" /><br><br>
							</c:when>
	
						    <c:otherwise>
							    <img src="No-Image-Placeholder.png" id="smallImage" /><br><br>
						 	</c:otherwise>
						 </c:choose>
						
						<!-- ADD / UPDATE EVIDENCE PIC FORM -->
						<form action="OrderController?action=addevidence" method="post" enctype="multipart/form-data">
						  	<input type="hidden" id="orderID" name="orderid" value="<c:out value="${order.orderid}"/>">
						  	<input type="file" name="evidencepic" accept="image/png, image/jpeg, image/jpg"><br><br>
						  	<input type="submit" class="buttons" value="Submit"><br>
						</form>
						<!------------------------------------->
				</div>		  
			  </div><br>
		</div>
	
	<script>
		//Script to open dan close side menu 
		function openNav() {
		  document.getElementById("mySidenav").style.width = "250px";
		  document.getElementById("header").style.marginLeft = "250px";
		  document.getElementById("body").style.marginLeft="250px";
		}
		
		function closeNav() {
		  document.getElementById("mySidenav").style.width = "0";
		  document.getElementById("header").style.marginLeft= "0";
		  document.getElementById("body").style.marginLeft="0";
		}
		</script>
	</body>
</html> 