<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("admin") != true)	  
      response.sendRedirect("AD_A_index.jsp");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Delivery Charge</title>
	<link rel="stylesheet" href="Admin/sidebar_ad.css">
	<link rel="stylesheet" href="Admin/header_ad.css">
	<link rel="stylesheet" href="Admin/body_ad.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		input[type=number], input[type=text], select {font-size: 17px;}
	</style>
	
	<%
	//Display alert message 
	String result = (String) request.getAttribute("result");
	
	if(result != null) 
	{
		if(result.equalsIgnoreCase("ucharge success")) //after add advertisement
		{
			%>
			<script type="text/javascript">
				alert("Delivery charge has been successfully updated");
			</script>
			<%
		}
	}
	%>
	
</head>
<body>
<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	
<div style="text-align:center"><span style="color:white ; "><b>ADMIN</b></span></div>
	 
	<img src="courier_logo.png" style="width:50%">
  
	<h2>List of Menus</h2>
	<a id="menu" href="OrderController?action=statistics"> Dashboard</a>
	<a id="menu" href="UserController?action=seniorlist"> Senior List</a>
	<a id="menu" href="ChargeController" class="active"> Update Delivery Charge</a>
	<a id="menu" href="UserController?action=empprofile"> Profile</a>
	<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
	<a id="menu" href="UserController?action=logout"> Logout</a>
	
</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">Update Delivery Charge</h1>
	
	<!--Instruction-->
	<div class="instruction">
		<h3>Update delivery charge on order and type</h3>
	</div>
	
	<hr class="hrStyle"><br>
	
	<!--Body-->
	
	
		<form id="form" action="ChargeController" method="POST">
		
		<div class="card">
			<h3 style="text-align:center;"><br>Details information of the delivery charge</h3>
			<div class="container" style="background:white;">
			<table style="text-align:left;">
			
			<c:set var="date" value="<%=new java.util.Date() %>" />
					
			<tr>
				<td style="width:20%;">
					<span class="border">Today</span>
				</td>
				<td>
					<input type="text" name="datechanged" value="<fmt:formatDate type="date" pattern="yyyy-MM-dd" value="${date}"/>" readonly>
				</td>
			</tr>
			<tr>					
				<td style="width:30%;">
					<span class="border">Charge on order </span>
				</td>
                <td>
					<input type="number" id="devText" name="chargeonorder" step="0.10" min="0.00" max="9.99" value="<c:out value="${charge.chargeonorder}"/>" placeholder="<c:out value="${charge.chargeonorder}"/>" required/>
					<!-- <span style="color:red;"> The charge cannot be changed expensive for the student!</span>-->
				</td>
			</tr>
			<tr>
                <td>
					<span class="border">Delivery types that need to charge RM1 </span>
				</td>
                <td>
                	<select id="chargeontype" name="chargeontype" style="width:20%;">
                		<option value="delivery" ${charge.chargeontype == 'delivery' ? 'selected="selected"': ''}>Delivery</option>
						<option value="meetup" ${charge.chargeontype == 'meetup' ? 'selected="selected"': ''}>Meetup</option>
					</select>
				</td>
			</tr>		
			<tr>
				<td></td>
				<td><button type="submit" id="add" name="Update" onclick="return confirm('Confirm this delivery charge?')" title="Button to update delvery charge">Update</button>
				<input type="button" id="Cancel" name="Cancel" style="margin-left:10px;" title="Button to go back" value="Cancel"/></td>
			</tr>
			
		</table> </div>
	</div>
	</form>
	
</div><br>


<script>

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
</body>
</html>