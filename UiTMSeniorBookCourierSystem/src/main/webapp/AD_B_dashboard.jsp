<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("admin") != true)	  
      response.sendRedirect("AD_A_index.jsp");
%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> <%-- take length from the list --%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Dashboard</title>
    <link rel="stylesheet" href="Admin/sidebar_ad.css">
	<link rel="stylesheet" href="Admin/header_ad.css">
	<link rel="stylesheet" href="Admin/body_ad.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		/*List*/ 
		.statdetails table {
			border-collapse: separate; border-spacing: 0; text-align: center;
			width:100%;
			border: 1px solid #cccccc;
		}
		
		.statdetails th {
			background-color: #2c3338; color: white;
			padding-top: 10px; padding-right: 10px; padding-bottom: 10px; padding-left: 10px;
		}
		
		.statdetails td {
			border-top: 1px solid #cccccc;
			padding-top: 15px; padding-right: 15px; padding-bottom: 15px; padding-left: 15px;
		}
	</style>
	
</head>
<body>
<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	
	<div style="text-align:center"><span style="color:white ; "><b>ADMIN</b></span></div>

	<img src="courier_logo.png" style="width:50%">
  
	<h2>List of Menus</h2>
	<a id="menu" href="OrderController?action=statistics" class="active"> Dashboard</a>
	<a id="menu" href="UserController?action=seniorlist"> Senior List</a>
	<a id="menu" href="ChargeController"> Update Delivery Charge</a>
	<a id="menu" href="UserController?action=empprofile"> Profile</a>
	<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
	<a id="menu" href="UserController?action=logout"> Logout</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;">Dashboard</h1>
	
	<!--Instruction-->
	<div class="instruction">
		<h3>Welcome to the dashboard</h3>
	</div>
	
	<hr class="hrStyle"><br>
	
	<!--Body-->
	<!--Statistics -->
	<div class="card">
    <h3 style="text-align:center;"><br>Statistics</h3>
		<div class="container" style="background:white;"><br>
			<table  style="text-align: center;">
				<tr bgcolor="#63bf79">
					<td width="1%" style="background-color: #ffffff;"></td>
					<td width="2%" style="padding-top: 5px; padding-bottom: 5px;">
						<i class="fa-solid fa-book fa-lg"></i>
					</td>
					<td width="2%" style="background-color: #ffffff;"></td>
					<td width="2%" style="padding-top: 5px; padding-bottom: 5px;">
						<i class="fa-solid fa-money-bill fa-lg"></i>
					</td>
					<td width="1%" style="background-color: #ffffff;"></td>
				</tr>
				
				<tr bgcolor="#f0f2f2">
					<td width="1%" bgcolor="white"></td>
					<td>
						<b>Total Book Sold</b><br><br>
						<c:out value="${adsDelivered.countadsdelivered}"/>
					</td>
					
					<td width="2%" bgcolor="white"></td>
					
					<td>
						<b>Profits<br><br>
						RM</b> <c:out value="${totalpriceDelivered.sumtpdelivered}"/>
					</td>
					<td width="1%" bgcolor="white"></td>

				</tr>
			</table>
		<br>
		</div>
	</div>
	<br><br>
	<div class="card">
    <h3 style="text-align:center;"><br>Details of Statistics</h3>
		<div class="container" style="background:white;"><br>
		<div class="statdetails">
			<%--Sum total price --%>
			<c:set var="total" value="${0}"/>
			<table border="1">
				<tr>
					<th>Senior</th>
					<th>Advertisement Delivered</th>
					<th>Total Price</th>
				</tr>
				<c:forEach items="${stats}" var="stat">
				<tr>
					<td>
						<b>ID:</b> <c:out value="${stat.ads.employees.empid}"/><br>
						<c:out value="${stat.ads.employees.name}"/></td>
					<td><c:out value="${stat.ads.adstitle}" /></td> 
					<td>
						<b>RM</b> <c:out value="${stat.totalprice}" />
						<c:set var="total" value="${total + stat.totalprice}"/>
					</td>
				</tr>
				</c:forEach>
				<tr>
					<td colspan="2" style="text-align:right;"><b>Profits</b></td>
					<td><b>RM</b> <c:out value="${total}" /></td>
				</tr>
			</table>
		</div>
		<br>
		</div>
	</div>
	<br>
	
</div>
<br>


<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>
</body>
</html>