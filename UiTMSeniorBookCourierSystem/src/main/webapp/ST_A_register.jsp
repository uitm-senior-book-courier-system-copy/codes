<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);
  %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Register</title>
	<link rel="stylesheet" href="Student/sidebar_st.css">
	<link rel="stylesheet" href="Student/header_st.css">
	<link rel="stylesheet" href="Student/body_st.css">
	
	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	
	<!-- Google Font -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Play&display=swap">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Heebo:wght@300&display=swap">
	
	<style>
		body{background: #332c38; color: white; font-family: 'Play', sans-serif;}
		.page-title {padding: 10px; text-align: center; background: #cc00cc; color: white; font-size: 30px;}
		a{text-decoration: none; color:#f678ff;}	
		a:hover{color: #a5aee6;}
	</style>
</head>
<body>
	<% String result = (String) request.getAttribute("result"); %>

	<%if (result != null) {%>
		<h4><%=result%></h4>
	<%}%>
	
	<a style="font-family: 'Heebo', sans-serif; font-weight: bold" href="ST_A_index.jsp">BACK</a>

	<br>
	<div align="center">
		<img src="courier_logo.png" alt="courier logo" height="150" width="150">

			<br><br><div class="page-title"><b>STUDENT REGISTER</b></div><br>
	</div>
	<br>
	<form action="UserController?action=register&" method="post">	
		<div align="center">
		<table style="width:80%">
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>MATRIC NUMBER</strong></td>
			<td><input name="matricnum" type="text" id="matricnum" required maxlength="10" size="10" style="background-color: white; margin: 11px"></td>
		</tr>
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>STUDENT PASSWORD</strong></td>
			<td><input name="studpassword" type="password" required maxlength="12" style="background-color: white; margin: 11px; width: 50%;"></td>
		</tr>
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>FIRST NAME</strong></td>
			<td><input name="firstname" type="text" required maxlength="100" style="background-color: white; margin: 11px"></td>
		</tr>
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>LAST NAME</strong></td>
			<td><input name="lastname" type="text" required maxlength="100" style="background-color: white; margin: 11px"></td>
		</tr>
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>GENDER</strong></td>
			<td style="padding-right: 11px; padding-left: 11px;">
			<select name="studgender" style="font-size:17px; padding-right: 11px; padding-left: 11px;">
				<option value='M'>Male</option>
				<option value='F'>Female</option>
			</select>
			</td>
		</tr>
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>PHONE NUMBER</strong></td>
			<td><input name="studphonenum" id="studphonenum" type="text" required maxlength="12" style="background-color: white; margin: 11px"></td>
		</tr>
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>EMAIL ADDRESS</strong></td>
			<td><input name="studemail" type="text" required maxlength="500" style="background-color: white; margin: 11px"></td>
		</tr>
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>COLLEGE HOUSE NUMBER</strong></td>
			<td><input name="collegehousenum" id="collegehousenum" type="text" required maxlength="4" size="4" style="background-color: white; margin: 11px"></td>
		</tr>
		<tr> 
			<td align="center" style="padding-right: 11px; padding-left: 11px;"><strong>COLLEGE BLOCK</strong></td>
			<td style="padding-right: 11px; padding-left: 11px;">
			<select name="collegeblock" style="font-size:17px; padding-right: 11px; padding-left: 11px;">
				<option value='tsla'>Tun Sri Lanang A (tsla)</option>
				<option value='tslb'>Tun Sri Lanang B (tslb)</option>
				<option value='tga'>Tun Gemala A (tga)</option>
				<option value='tgb'>Tun Gemala B (tgb)</option>
				<option value='tgc'>Tun Gemala C (tgc)</option>
			</select>
			</td>
		</tr>
		<tr>
			<td colspan="3">
			<div align="center">
				<button type="submit" name="Submit" class="buttons" value="Submit">Submit</button>
			</div>
			</td>
		</tr>
		</table>
		</div>
	</form>
	
<script>
	$(document).ready(function()
	{
		$('button[type="submit"]').click(function()
		{	
			var m = $("#matricnum").val();
			var c = $("#collegehousenum").val();
			var p = $("#studphonenum").val();
			
			var r_matric = $.isNumeric(m);
			var r_phonenum = $.isNumeric($("#studphonenum").val());
			var r_housenum = $.isNumeric(c);
			
			if (r_matric == true && r_phonenum == true && r_housenum == true) {
				
				 if ( m.length == 10 && c.length == 4 ) {
					 
					 if(p.length >= 10 && p.length <= 12) {
						if(confirm("Confirm this data?"))
							return true;
						else
							return false;
					}
					else {
						alert("PHONE NUMBER should be between 10-12 length!");
						return false;
					}
				 }
				 else {
					alert("MATRIC NUMBER should be in 10 length AND\nCOLLEGE HOUSE NUMBER should be in 4 length!");
					$('#matricnum').css("background-color","#ffb3b3");
					$('#collegehousenum').css("background-color","#ffb3b3");
					return false;
				 }
			}
			else {
				alert("Insert using the right format!\n\nMATRIC NUMBER, PHONE NUMBER and COLLEGE HOUSE NUMBER should be in numeric values!");
				$('#matricnum').css("background-color","#ffb3b3");
				$('#studphonenum').css("background-color","#ffb3b3");
				$('#collegehousenum').css("background-color","#ffb3b3");
				return false;
			}
		})
	});
</script>
</body>
</html>