<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int empid = (Integer) session.getAttribute("id");
  String name = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  
  if(role.equalsIgnoreCase("admin") != true)	  
      response.sendRedirect("AD_A_index.jsp");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Edit Profile</title>
<%
	//Display alert message 
	String alert = (String) request.getAttribute("getAlert");
	
	if(alert != null) 
	{
		if(alert.equalsIgnoreCase("Update Success")) //after add advertisement
		{
			%>
			<script type="text/javascript">
				alert("Profile has been updated successfully");
			</script>
			<%
		}
		if(alert.equalsIgnoreCase("UpdatePs Success")) //after add advertisement
		{
			%>
			<script type="text/javascript">
				alert("Password has been updated successfully");
			</script>
			<%
		}
		if(alert.equalsIgnoreCase("same password")) //after add advertisement
		{
			%>
			<script type="text/javascript">
				alert("You cannot use the old password as the new password!\n\nTry Again.");
			</script>
			<%
		}
	}
%>

	 <link rel="stylesheet" href="Admin/sidebar_ad.css">
	<link rel="stylesheet" href="Admin/header_ad.css">
	<link rel="stylesheet" href="Admin/body_ad.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
	
	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	
	<style>
		body {font-family: "Lato", sans-serif; background: white;}
		table,td,tr,th {border:0px;}	
		input[type=email], input[type=password], input[type=text], #pass {
		  width: 20%; padding: 12px 20px; margin: 8px 0; display: inline-block;
		  border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box;
		}
		
		input[type=text], input[type=email] {margin-left:20px;}
		
		#UpdatePs, #update {
			background-color: #4475fc; border-radius: 4px; border: none; color: white; padding: 10px 40px;
			text-align: center; text-decoration: none; display: inline-block; font-size: 17px; margin-left:20px;
		}
		
		select{
		  width: 20%; padding: 12px 20px; margin: 8px 0; display: inline-block;
		  border: 1px solid #ccc; border-radius: 4px; box-sizing: border-box; margin-left:20px;
		}
		
	</style>
		
	<script>
	
	$(document).ready(function()
	{
		$('button[name="Update"]').click(function()
		{	
			var r_phonenum = $.isNumeric($("#empphonenum").val());
			var p = $("#empphonenum").val();
			
			if (r_phonenum == true) {
				
				if(p.length >= 10 && p.length <= 12) {
					if(confirm("Confirm this profile data?"))
						return true;
					else
						return false;
				}
				else {
					alert("PHONE NUMBER should be between 10-12 length!");
					$('#empphonenum').css("background-color","#ffb3b3");
					return false;
				}
			}
			else {
				alert("Insert using the right format!\n\nPHONE NUMBER should be in numeric values!");
				$('#empphonenum').css("background-color","#ffb3b3");
				return false;
			}
		})
		
		$('button[name="UpdatePs"]').click(function()
		{	
			var sp = $("#emppassword").val();
			var csp = $("#confirmemppassword").val();
			
			if (sp != csp) {
				alert("Confirm new password not same with new password!");
				$('#confirmemppassword').css("background-color","#ffb3b3");
				return false;
			}
			else
				if(confirm("Confirm this password data?"))
					 return true;
				 else
					 return false;
		})
	});
	
	</script>

</head>
<body>
<!-- SIDE NAVIGATION -->
<div id="mySidenav" class="sidenav">
  
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	

	<div style="text-align:center"><span style="color:white ; "><b>ADMIN</b></span></div>
	
	

	 <img src="courier_logo.png" style="width:50%">
  
	<h2>List of Menus</h2>
	<a id="menu" href="OrderController?action=statistics"> Dashboard</a>
	<a id="menu" href="UserController?action=seniorlist"> Senior List</a>
	<a id="menu" href="ChargeController"> Update Delivery Charge</a>
	<a id="menu" href="UserController?action=empprofile" class="active"> Profile</a>
	<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
	<a id="menu" href="UserController?action=logout"> Logout</a>

</div>

<!-- HEADER -->
<div id="header">

   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
   <span style="float:right;color: #ffffff;">Welcome,<br><%=name %> | <%=role %></span>

</div>

<!-- CONTENT -->
<div id="body">

	<!--Title -->
	<h1 style="color:black;"> Edit Profile Information </h1>
	
	<!--Instruction-->
	<div class="instruction">
		<h3>Update the latest profile information</h3>
	</div>
	
	<hr class="hrStyle"><br>
	<!--Body-->
	
		<!-- PROFILE INFORMATION -->
		<form id="form" action="UserController?action=updateEprofile" method="POST">	
		
			<div class="card">
				<h3 style="text-align:center;"><br>Profile information</h3>
				<div class="container" style="background:white;">
					<table style="text-align:center-right;">
							
						<tr>
			                <td>
								<span class="border">Username  </span>
							</td>
			                <td>
								<input type="text" id="username" name="username"  size="50" value="<c:out value="${emp.username}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">Name</span>
							</td>
			                <td>
								<input type="text" id="name" name="name" size="200" value="<c:out value="${emp.name}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">Gender </span>
							</td>
							<td>
								<select name="empgender" style="font-size:17px; padding-right: 11px; padding-left: 11px;">
									<c:if test="${fn:containsIgnoreCase(emp.empgender, 'F')}">
										<option value='M'>Male</option>
										<option value='F' selected>Female</option>
							      	</c:if>
							      	<c:if test = "${fn:containsIgnoreCase(emp.empgender, 'M')}">
							         	<option value='M' selected>Male</option>
										<option value='F'>Female</option>
							      	</c:if>
								</select>
							</td>
						</tr>
						
						<tr>
			                <td>
								<span class="border">Phone Number  </span>
							</td>
			                <td>
								<input type="text" id="empphonenum" name="empphonenum"  size="12"  maxlength="12" value="<c:out value="${emp.empphonenum}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">Email  </span>
							</td>
			                <td>
								<input type="email" id="empemail" name="empemail"  size="255" value="<c:out value="${emp.empemail}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required>
							</td>
						</tr>
						
						<tr>
							<td>
								<span class="border">Role  </span>
							</td>
			                <td>
								<input type="text" id="role" name="role"  size="255" value="<c:out value="${emp.role}"/>" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" disabled>
							</td>
						</tr>
						
						<tr>
							<td></td>
							<td><button type="submit" id="update" name="Update" title="Button to update data into database" style="cursor:pointer;">Update Profile</button></td>
						</tr>
					
					</table> 
			 </div>
		 </div>
	</form>
	
	<br><br>
	
	<!-- ////////////////////////////////////////////////////////// -->
	<!-- PASSWORD -->
	<form id="form" action="UserController?action=updateEprofile" method="POST">	
		
		<div class="card">
			<h3 style="text-align:center;"><br>Edit New Password</h3>
			<div class="container" style="background:white;">
				<table style="text-align:center-right;">
					<tr>
						<td><span class="border">New Password</span></td>
		                <td><input type="password" id="emppassword" name="emppassword"  size="50" maxlength = "12" style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required></td>
					</tr>
					
					<tr>
						<td><span class="border">Confirm New Password</span></td>
		                <td><input type="password" id="confirmemppassword" name="confirmemppassword"  size="50" maxlength = "12"  style="resize:none;width:60%;font-size:17px;font-family: 'Lato', sans-serif;" required></td>
					</tr>
					
					<tr>
						<td></td>
						<td><button type="submit" id="update" name="UpdatePs" title="Button to update password into database" style="cursor:pointer;">Update Password</button></td>
					</tr>
				</table> 
		 </div>
	 </div>
</form>
	
</div><br>


<script>
//Script to open dan close side menu 
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("header").style.marginLeft = "250px";
  document.getElementById("body").style.marginLeft="250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("header").style.marginLeft= "0";
  document.getElementById("body").style.marginLeft="0";
}
</script>

</body>
</html>