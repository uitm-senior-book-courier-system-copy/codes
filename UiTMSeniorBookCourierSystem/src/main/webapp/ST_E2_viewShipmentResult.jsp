<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);

  int studid = (Integer) session.getAttribute("id");
  String firstname = (String) session.getAttribute("name");
  String role = (String) session.getAttribute("role");
  String studgender = (String) session.getAttribute("studgender");
  
  if(role.equalsIgnoreCase("student") != true)	  
      response.sendRedirect("ST_A_index.jsp");
  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="ISO-8859-1">
	<title>Details of Shipment</title>
		
		<link rel="stylesheet" href="Student/sidebar_st.css">
		<link rel="stylesheet" href="Student/header_st.css">
		<link rel="stylesheet" href="Student/body_st.css">
		
		<style>
			body {font-family: "Lato", sans-serif; background: white;}
		</style>
	</head>
	<body>
	
	<!-- SIDE NAVIGATION -->
	<div id="mySidenav" class="sidenav">
	  
		<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
		
		<div align="center">
			<span style="color:white;"><b>STUDENT</b></span>
		</div>
	
		<img src="courier_logo.png" style="width:50%">
	  
		<h2>List of Menus</h2>
		<a id="menu" href="ST_B_dashboard.jsp"> Dashboard</a>
		<a id="menu" href="UserController?action=studentprofile"> Profile</a>
		<a id="menu" href="AdvertisementController?action=adslist"> List of Advertisement</a>
		<a id="menu" href="OrderController?action=studorderlist" class="active"> View Shipment</a>
		<a id="menu" href="UserController?action=logout"> Logout</a>
	
		</div>
	
		<!-- HEADER -->
		<div id="header">
	
		   <h3 id="user" align="center" style="margin-left:1.5%;">Welcome to UITM SENIOR BOOK COURIER SYSTEM!</h3>
		   <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; </span>
		   <span style="float:right;color: #ffffff;">Welcome,<br><%=firstname %> | <%=role %></span>
	
		</div>
	
		<!-- CONTENT -->
		<div id="body">
	
			<!--Title -->
			<h1 style="color:black;">Details of the selected Shipment</h1>
			
			<!--Instruction-->
			<div class="instruction">
				<h3>You can view the parcel evidence </h3>
			</div>
			
			<hr class="hrStyle"><br>
			
			<!--Body-->
			<div align="center">
				<div id="container2" align="center" style="height:50%">
					
					<c:set var="typeofdelivery" value="${order.typeofdelivery}" /> 
					<c:set var="ucase" value="${fn:toUpperCase(fn:substring(typeofdelivery, 0, 1))}" /> 
					<c:set var="lcase" value="${fn:toLowerCase(fn:substring(typeofdelivery, 1,fn:length(typeofdelivery)))}" /> 
					
					<!-- Display details of order based on orderid entered -->
					<div id="label"><b>VIEW SHIPMENT</b></div>
					<br><br> 
						
					<label for="orderID">Order Id :</label>
					<input type="text" id="orderID" name="orderid" value="<c:out value="${order.orderid}"/>" readonly><br><br>
					
					<label for="orderID">Order Date :</label>
					<input type="text" id="orderID" name="orderid" value="<fmt:formatDate dateStyle="long" value="${order.orderdate}" />" readonly><br><br>  
					
					<label for="Pname">Product Name :</label><br>
					<textarea id="Pname" name="Pname" rows="2" cols="50" readonly><c:out value="${order.ads.adstitle}"/></textarea><br><br>
					  
					<label for="price">Total Price:</label>
					<input type="text" id="price" name="price" value="RM <fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${order.totalprice}"/>" readonly><br><br>
					
					<label for="Pname">Senior In Charge:</label><br>
					<textarea id="Pname" name="Pname" rows="3" cols="50" readonly><c:out value="${order.ads.employees.name}"/> (<c:out value="${order.ads.employees.empphonenum}"/>) (<c:out value="${order.ads.employees.empemail}"/>)</textarea><br><br>  
					
					<label for="ship"><c:out value="${ucase}${lcase}"/> Date:</label>
					<input type="text" id="ship" name="ship" value="<fmt:formatDate dateStyle="long" value="${order.mddate}" />" readonly><br><br>  
					
					<label for="ship">Place & Time:</label>
					<input type="text" id="ship" name="ship" value="<c:out value="${order.mdplace}"/>, <c:out value="${order.mdtime}"/>" readonly><br><br>  
					
					<label for="ship">Shipment Status:</label>
					<input type="text" id="ship" name="ship" value="<c:out value="${order.status}"/>" readonly><br><br>
					
					<label for="ship">College:</label>
					<input type="text" id="ship" name="ship" value="<c:out value="${order.students.collegehousenum}"/> <c:out value="${order.students.collegeblock}"/>" readonly><br><br>
					
					<label for="evidence">Parcel Evidence:</label><br>
					<c:if test = "${not empty order.evidencepic}">
						<img src="data:image/jpg;base64,${order.evidencepic}" id="smallImage" />
			      	</c:if>
			      	<c:if test = "${empty order.evidencepic}">
			         	<img src="No-Image-Placeholder.png" id="smallImage" />
			      	</c:if>
					<!------------------------------------------------------>
					<br>
			  </div>
		  </div>
		  
		</div>
		<br>
	
	
	<script>
		//Script to open dan close side menu 
		function openNav() {
		  document.getElementById("mySidenav").style.width = "250px";
		  document.getElementById("header").style.marginLeft = "250px";
		  document.getElementById("body").style.marginLeft="250px";
		}
		
		function closeNav() {
		  document.getElementById("mySidenav").style.width = "0";
		  document.getElementById("header").style.marginLeft= "0";
		  document.getElementById("body").style.marginLeft="0";
		}
	</script>
	
	</body>
</html>