<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);
  %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Student Login</title>
	<link rel="stylesheet" href="Student/sidebar_st.css">
	<link rel="stylesheet" href="Student/header_st.css">
	<link rel="stylesheet" href="Student/body_st.css">
	
	<!-- Google Font -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Play&display=swap">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Doppio One'>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Heebo:wght@300&display=swap">
	
	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
	
	<style>
		body{background: #332c38; color: white; font-family: 'Play', sans-serif;}
		
		img{width:260px; height:260px;}
		
		hr{border: 1px solid white; width: 550px;}
		
		/* link to the student index.php */
		a{text-decoration: none; color:#f678ff;}	
		a:hover{color: #a5aee6;}
				
		input[type=text], input[type=password] 	/* username , ic number */
		{
			width: 25%;	margin: 10px 0;	box-sizing: border-box;
			padding: 10px;	font-family: 'Doppio One';	font-size: 18px;
		}	
	</style>
	
	<script>
	$(document).ready(function()
	{
		$('input[type="submit"]').click(function()
		{	
			var r_matric = $.isNumeric($("#matricnum").val());
			
			if ( $("#matricnum").val().length == 10) {
				 
				if (r_matric == true) {
					return true;
				}
				else {
					alert("Insert using the right format!\n\nmatricnum should be in numeric values!");
					$('#matricnum').css("background-color","#ffb3b3");
					return false;
				}
			 }
			 else {
				alert("MATRIC NUMBER should be in 12 length!");
				$('#matricnum').css("background-color","#ffb3b3");
				return false;
			 }
		})
	});
	</script>
		
</head>

<body>

	<% String result = (String) request.getAttribute("result"); %>

	<%if (result != null) {%>
		<h4><%=result%></h4>
	<%}%>
	
	<a style="font-family: 'Heebo', sans-serif; font-weight: bold" href="A_index.jsp">BACK</a>

	<div align="center">
		<img src="courier_logo.png" alt="CourierSystem Logo"><br>
		<h1>UITM SENIOR BOOK COURIER SYSTEM</h1>
		<hr>
		<h3 style="font-family: 'Heebo', sans-serif;">Student Login</h3>
	
		<form id="login" name="login" method="post" action="UserController?action=studlogin&">
			<input type="text" name="matricnum" id="matricnum" placeholder="matricnum" maxlength = "10" required/><br><br>
			<input type="password" name="studpassword" id="studpassword" placeholder="Password" maxlength = "12" required><br><br>
			
			<input type="submit" class="btnindx" name="btnlogin" value="Log in"><br><br>
		</form>
		
		<a style="font-family: 'Heebo', sans-serif; font-weight: bold" href="ST_A_register.jsp">Register New!</a>
		
	</div>

</body>
	
</html>