<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
  response.addHeader("Pragma", "no-cache");
  response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
  response.addHeader("Cache-Control", "pre-check=0, post-check=0");
  response.setDateHeader("Expires", 0);
  %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Login</title>
	<link rel="stylesheet" href="Admin/sidebar_ad.css">
	<link rel="stylesheet" href="Admin/header_ad.css">
	<link rel="stylesheet" href="Admin/body_ad.css">
	
	<!-- Google Font -->
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Play&display=swap">
	<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Doppio One'>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Heebo:wght@300&display=swap">
	
	<style>
		body{background: #332c38; color: white; font-family: 'Play', sans-serif;}
		
		img{width:200px;}
		
		hr{border: 1px solid white; width: 550px;}
		
		/* link to the student index.php */
		a{text-decoration: none; color:#f678ff;}	
		a:hover{color: #a5aee6;}
				
		input[type=text], input[type=password] 	/* username , ic number */
		{
			width: 25%;
			margin: 10px 0;
			box-sizing: border-box;
			padding: 10px;
			font-family: 'Doppio One';
			font-size: 18px;
		}
	</style>
</head>
<body>

	<% String result = (String) request.getAttribute("result"); %>

	<%if (result != null) { %>
		<h4><%=result%></h4>
	<%}%>
	
	<a style="font-family: 'Heebo', sans-serif; font-weight: bold" href="A_index.jsp">BACK</a>
	
	<div align="center">
		<img src="courier_logo.png" alt="Logo" class="center"><br>
		<h1>UITM SENIOR BOOK COURIER SYSTEM</h1>
		<hr>
		<h3 style="font-family: 'Heebo', sans-serif;"> Admin Login</h3>
		
		<form id="login" name="login" method="post" action="UserController?action=emplogin&">
			<input type="text" name="username" id="username" placeholder="Username" maxlength = "50" required/><br><br>
			<input type="password" name="emppassword" placeholder="Password" maxlength = "12" required><br><br>
			<input type="submit" class="btn" name="btnlogin" value="Log in"><br><br>
		</form>
		
</div>
</body>
</html>