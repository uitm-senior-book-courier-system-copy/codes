package system.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;

import system.connection.ConnectionManager;
import system.model.Charge;

public class ChargeDAO {
	
	static Connection con = null;
	static Statement stmt = null;
	static PreparedStatement ps = null;
	static ResultSet rs = null;
	
	private int empid;
	private double chargeonorder;
	private String chargeontype;
	private Date datechanged = new Date();
	
	//CRUD or any other methods
	
	/* ---------------------------------- READ SINGLE LATEST CHARGE -------------------------------------- */
	public static Charge getCharge() {
		
		Charge c = new Charge();
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			String sql = "SELECT * FROM charges ORDER BY datechanged DESC LIMIT 1;";
			ps = con.prepareStatement(sql);

			rs = ps.executeQuery(); //4.execute query
			if(rs.next()) {
				c.setChargeonorder(rs.getDouble("chargeonorder"));
				c.setChargeontype(rs.getString("chargeontype"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

		return c;
	}
	/* ---------------------------------- END OF READ SINGLE LATEST CHARGE ---------------------------------- */
	
	
	/* ----------------------------------------- ADD CHARGE ------------------------------------------ */
	public void addCharge(Charge bean) {

		chargeonorder  = bean.getChargeonorder();
		chargeontype = bean.getChargeontype();
		datechanged = bean.getDatechanged();
		empid = bean.getEmpid();
		
		java.sql.Date cdate = new java.sql.Date(datechanged.getTime()); //convert date

		try {			
			//call getConnection() method
			con = ConnectionManager.getConnection();

			//3. create statement
			ps = con.prepareStatement("INSERT INTO charges (chargeonorder,chargeontype,datechanged,empid) VALUES(?,?,?,?)");				
			ps.setDouble(1, chargeonorder);
			ps.setString(2, chargeontype);
			ps.setDate(3, cdate);
			ps.setInt(4, empid);

			//4. execute query
			ps.executeUpdate();

			//5. close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();				
		}
	}
	/* ----------------------------------------- END OF ADD CHARGE ------------------------------------------ */

}
