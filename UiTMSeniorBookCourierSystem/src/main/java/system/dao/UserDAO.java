package system.dao;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import system.connection.ConnectionManager;
import system.model.User;

public class UserDAO {
	
	static Connection con = null;
	static Statement stmt = null;
	static PreparedStatement ps = null;
	static ResultSet rs = null;
	
	//EMPLOYEES (SENIOR/ADMIN)
	private static int empid;
	private static String emppassword, username, name, empphonenum, empemail, role, empgender;
	
	//STUDENTS
	private static int studid; 
	private static String matricnum, studpassword, firstname, lastname, studphonenum, studemail, collegehousenum, collegeblock, studgender;
	
	//CRUD or any other methods
	
	
	/* ------------------------------------------- READ LIST -------------------------------------------- */
	//STUDENTS
	public static List<User> getAllStudents() {
		
		List<User> users = new ArrayList<User>();

		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();
			//3. create statement
			stmt = con.createStatement();
			String sql = "SELECT * FROM students";
			//4.execute query
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				User user = new User(); //from user.model package --> User.java
				
				user.setStudid(rs.getInt("studid"));
				user.setMatricnum(rs.getString("matricnum"));
				user.setStudpassword(rs.getString("studpassword"));
				user.setFirstname(rs.getString("firstname"));
				user.setLastname(rs.getString("lastname"));
				user.setStudgender(rs.getString("studgender"));
				user.setStudphonenum(rs.getString("studphonenum"));
				user.setStudemail(rs.getString("studemail"));
				user.setCollegehousenum(rs.getString("collegehousenum"));
				user.setCollegeblock(rs.getString("collegeblock"));

				users.add(user);
			}
			//5.close connection
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}

		return users;
	}
	
	//SENIORS
	public static List<User> getAllSeniors() {
		
		List<User> seniors = new ArrayList<User>();
		
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();
			//3. create statement
			stmt = con.createStatement();
			String sql = "SELECT * FROM employees where role='senior'";
			//4. execute query
			rs = stmt.executeQuery(sql);
			while(rs.next()) {
				User senior = new User();
				
				senior.setEmpid(rs.getInt("empid"));
				senior.setEmppassword(rs.getString("emppassword"));
				senior.setUsername(rs.getString("username"));
				senior.setName(rs.getString("name"));
				senior.setEmpgender(rs.getString("empgender"));
				senior.setEmpphonenum(rs.getString("empphonenum"));
				senior.setEmpemail(rs.getString("empemail"));	
				senior.setRole(rs.getString("role"));
				
				seniors.add(senior);
			}			
			//5.close connection
			con.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return seniors;
	}
	/* --------------------------------------- END OF READ LIST -------------------------------------------- */
	
	
	/* ----------------------------------------- READ SINGLE --------------------------------------- */
	//STUDENT
	public static User getStudById(int studid)
	{
		User stud = new User();
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			//3. create statement
			String sql = "SELECT * FROM students WHERE studid=?";
			ps = con.prepareStatement(sql);
			ps.setInt(1, studid);

			//4.execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				stud.setStudid(rs.getInt("studid"));
				stud.setMatricnum(rs.getString("matricnum"));
				stud.setStudpassword(rs.getString("studpassword"));
				stud.setFirstname(rs.getString("firstname"));
				stud.setLastname(rs.getString("lastname"));
				stud.setStudgender(rs.getString("studgender"));
				stud.setStudphonenum(rs.getString("studphonenum"));
				stud.setStudemail(rs.getString("studemail"));
				stud.setCollegehousenum(rs.getString("collegehousenum"));
				stud.setCollegeblock(rs.getString("collegeblock"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}

		return stud;
	}
	
	//EMPLOYEE (SENIOR & ADMIN)
	public static User getEmpById(int empid) {
		User emp = new User();
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();
			
			//3. create statement
			String sql = "SELECT * FROM employees WHERE empid=?";
			ps = con.prepareStatement(sql);
			ps.setInt(1, empid);
			
			//4.execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				emp.setEmpid(rs.getInt("empid"));
				emp.setEmppassword(rs.getString("emppassword"));
				emp.setUsername(rs.getString("username"));
				emp.setName(rs.getString("name"));
				emp.setEmpgender(rs.getString("empgender"));
				emp.setEmpphonenum(rs.getString("empphonenum"));
				emp.setEmpemail(rs.getString("empemail"));
				emp.setRole(rs.getString("role"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return emp;
	}
	/* ----------------------------------------- END OF READ SINGLE --------------------------------------- */
	
	
	/* ----------------------------------------- LOGIN ----------------------------------------------- */
	//STUDENT
	public static User studlogin(User bean) throws NoSuchAlgorithmException {

		//retrieve email and password from LoginController
		matricnum = bean.getMatricnum();
		studpassword = bean.getStudpassword();

		//convert password to MD5
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(studpassword.getBytes());
		byte byteData[] = md.digest();

		//convert byte to hex format
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i<byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100,16).substring(1));
		}

		//WAJIB ADA
		try {
			con = ConnectionManager.getConnection();

			//3. create statement
			String sql = "SELECT * FROM students WHERE matricnum = '"+matricnum+"' AND studpassword = '"+sb.toString()+"'";
			stmt = con.createStatement();

			//4. execute query
			rs = stmt.executeQuery(sql);
			boolean more = rs.next();//check if there is more

			if(more) {
				bean.setStudid(rs.getInt("studid"));
				bean.setFirstname(rs.getString("firstname"));
				bean.setStudgender(rs.getString("studgender"));
			}

			//5.close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();
		}

		return bean;
	}
	
	//EMPLOYEE
	public static User emplogin(User bean) throws NoSuchAlgorithmException {
		
		username = bean.getUsername();
		emppassword = bean.getEmppassword();

		//convert password to MD5
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(emppassword.getBytes());
		byte byteData[] = md.digest();

		//convert byte to hex format
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i<byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100,16).substring(1));
		}

		//WAJIB ADA
		try {
			con = ConnectionManager.getConnection();

			//3. create statement
			String sql = "SELECT * FROM employees WHERE username = '"+username+"' AND emppassword = '"+sb.toString()+"'";
			stmt = con.createStatement();

			//4. execute query
			rs = stmt.executeQuery(sql);
			boolean more = rs.next();//check if there is more

			if(more) {
				bean.setEmpid(rs.getInt("empid"));
				bean.setName(rs.getString("name"));
				bean.setRole(rs.getString("role"));
				bean.setEmpvalid(true);
			}
			else {
				bean.setEmpvalid(false);
			}

			//5.close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();
		}

		return bean;
	}
	/* ----------------------------------------- END OF LOGIN ----------------------------------------------- */
	
	
	/* ---------------------------------------- ADD STUDENT (REGISTER) -------------------------------------- */
	//CHECK STUDENT MATRICNUM BEFORE CAN REGISTER OR NOT
	public boolean checkStudentReg (User bean) {
		
		matricnum = bean.getMatricnum();
		
		boolean similar = false;
		
		try {
			con = ConnectionManager.getConnection();
			
			//3. create statement
			String sql = "SELECT * FROM students WHERE matricnum=?";
			ps = con.prepareStatement(sql);
			ps.setString(1, matricnum);
			
			//4.execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				similar = true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return similar;
	}
	
	//REGISTER STUDENT
	public void registerStudent (User bean) {

		//retrieve from controller
		matricnum = bean.getMatricnum();
		studpassword = bean.getStudpassword();
		firstname = bean.getFirstname();
		lastname = bean.getLastname();
		studgender = bean.getStudgender();
		studphonenum = bean.getStudphonenum();
		studemail = bean.getStudemail();
		collegehousenum = bean.getCollegehousenum();
		collegeblock = bean.getCollegeblock();

		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();
			
			//3. create statement
			String sql = "INSERT INTO students(matricnum,studpassword,firstname,lastname,studgender,studphonenum,studemail,collegehousenum,collegeblock)"
					+ "VALUES(?,MD5(?),?,?,?,?,?,?,?)"; //? for marker
			ps = con.prepareStatement(sql);
			ps.setString(1, matricnum);
			ps.setString(2, studpassword);
			ps.setString(3, firstname);
			ps.setString(4, lastname);
			ps.setString(5, studgender);
			ps.setString(6, studphonenum);
			ps.setString(7, studemail);
			ps.setString(8, collegehousenum);
			ps.setString(9, collegeblock);
			
			//4. execute query
			ps.executeUpdate();

			//5.close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	/* ------------------------------------ END OF ADD STUDENT (REGISTER) ------------------------------------ */
	
	
	/* ------------------------------------ CRUD SENIOR (ADD, DELETE) -------------------------------------- */
	//CHECK SENIOR USERNAME BEFORE CAN ADD OR NOT
	public boolean checkSeniorAdd (String username) {
		
		boolean similar = false;
		
		try {
			con = ConnectionManager.getConnection();
			
			//3. create statement
			String sql = "SELECT * FROM employees WHERE username=?";
			ps = con.prepareStatement(sql);
			ps.setString(1, username);
			
			//4.execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				similar = true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return similar;
	}
	
	//ADD SENIOR
	public void AddSenior (User bean) {
		
		//retrieve from controller
		emppassword = bean.getEmppassword();
		username = bean.getUsername();
		name = bean.getName();
		empgender = bean.getEmpgender();
		empphonenum = bean.getEmpphonenum();
		empemail = bean.getEmpemail();
		role = bean.getRole();
		
		try {
			con = ConnectionManager.getConnection();
			
			//3. create statement
			String sql = "INSERT INTO employees(emppassword,username,name,empgender,empphonenum,empemail,role)VALUES(MD5(?),?,?,?,?,?,?)";
			ps = con.prepareStatement(sql);
			ps.setString(1, emppassword);	
			ps.setString(2, username);		
			ps.setString(3, name);	
			ps.setString(4, empgender);
			ps.setString(5, empphonenum);
			ps.setString(6, empemail);
			ps.setString(7, role);
			
			//4. execute query
			ps.executeUpdate();
			
			//5.close connection
			con.close();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//DELETE SENIOR
	public void deleteSenior (int empid) {
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			//3. create statement
			String sql = "DELETE FROM employees WHERE empid=?";
			ps = con.prepareStatement(sql);			
			ps.setInt(1, empid);

			//4. execute query
			ps.executeUpdate();

			//5.close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	/* ------------------------------------ END OF CRUD SENIOR (ADD, DELETE) -------------------------------- */
	
	
	/* ----------------------------------- UPDATE PROFILE & PASSWORD ----------------------------------------- */
	//STUDENT PROFILE
	public void updateSTProfile(User bean) {

		studid = bean.getStudid();
		firstname = bean.getFirstname();
		lastname = bean.getLastname();
		studgender = bean.getStudgender();
		studphonenum = bean.getStudphonenum();
		studemail = bean.getStudemail();
		collegehousenum = bean.getCollegehousenum();
		collegeblock = bean.getCollegeblock();
		
		String sql ="UPDATE students SET firstname='"+firstname+"',"
				+ "lastname='"+lastname+"',studgender='"+studgender+"',"
				+ "studphonenum='"+studphonenum+"',studemail='"+studemail+"',"
				+ "collegehousenum='"+collegehousenum+"',collegeblock='"+collegeblock+"' WHERE studid='"+studid+"'";

		try {
			//call getConnection() method 
			con = ConnectionManager.getConnection();
			//3. create statement 
			stmt = con.createStatement();
			//4. execute query
			stmt.executeUpdate(sql);
			//5. close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();

		}
	}
	
	//EMPLOYEE PROFILE
	public void updateEProfile(User bean) {

		empid = bean.getEmpid();
		username = bean.getUsername();
		name = bean.getName();
		empgender = bean.getEmpgender();
		empphonenum = bean.getEmpphonenum();
		empemail = bean.getEmpemail();

		String sql ="UPDATE employees SET username='"+username+"',"
				+ "name='"+name+"',empgender='"+empgender+"',"
				+ "empphonenum='"+empphonenum+"',empemail='"+empemail+"' WHERE empid='"+empid+"'";

		try {
			//call getConnection() method 
			con = ConnectionManager.getConnection();
			//3. create statement 
			stmt = con.createStatement();
			//4. execute query
			stmt.executeUpdate(sql);
			//5. close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();

		}
	}
	
	//CHECK PASSWORD WITH PREVIOUS
	public boolean checkPsPre(String password, String role, int id) {

		String sql;
		
		if(role.equalsIgnoreCase("student")) {
			sql = "SELECT studpassword FROM students WHERE studpassword=MD5(?) AND studid=?";
		}
		else { //senior and admin
			sql = "SELECT emppassword FROM employees WHERE emppassword=MD5(?) AND empid=?";
		}
		
		boolean same = false;
		
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();
			
			//3. create statement
			ps = con.prepareStatement(sql);
			ps.setString(1, password);
			ps.setInt(2, id);
			
			//4.execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				same = true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return same;
	}
	
	//STUDENT AND EMPLOYEES PASSWORD
	public void updatePassword(User bean, String role) {

		String sql = "";
		if(role.equalsIgnoreCase("student")) {
			studpassword = bean.getStudpassword();
			studid = bean.getStudid();

			sql ="UPDATE students SET studpassword=MD5('"+studpassword+"') WHERE studid='"+studid+"'";
		}
		else { //senior and admin
			emppassword = bean.getEmppassword();
			empid = bean.getEmpid();

			sql ="UPDATE employees SET emppassword=MD5('"+emppassword+"') WHERE empid='"+empid+"'";
		}
		
		try {
			//call getConnection() method 
			con = ConnectionManager.getConnection();
			//3. create statement 
			stmt = con.createStatement();
			//4. execute query
			stmt.executeUpdate(sql);
			//5. close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();

		}
	}
	/* ----------------------------------- END OF UPDATE PROFILE & PASSWORD ---------------------------------- */

}
