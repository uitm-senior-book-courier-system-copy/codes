package system.dao;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import jakarta.servlet.http.Part;
import system.connection.ConnectionManager;
import system.model.Advertisement;

public class AdvertisementDAO {
	
	static Connection con = null;
	static Statement stmt = null;
	static PreparedStatement ps = null;
	static ResultSet rs = null;
	
	private static int adsid, empid;
	private static String adstitle, adsdesc;
	private static Part adspicture;
	private static double price;
	
	//CRUD or any other methods
	
	
	/* ---------------------------------- READ LIST ADVERTISEMENT ------------------------------------ */
	//WHEN ADMIN & STUDENT
	public static List<Advertisement> getAllAds() {
		
		List<Advertisement> ads = new ArrayList<Advertisement>();

		try {
			con = ConnectionManager.getConnection();

			stmt = con.createStatement(); //create statement

			String sql = "SELECT * FROM advertisements a INNER JOIN employees e ON a.empid=e.empid";
			rs = stmt.executeQuery(sql); //execute query

			//retrieve the data using while loop
			while(rs.next()) {
				Advertisement ad = new Advertisement(); //create object

				ad.setAdsid(rs.getInt("adsid"));
				ad.setAdstitle(rs.getString("adstitle"));
				ad.setAdsdesc(rs.getString("adsdesc"));
				ad.setPrice(rs.getDouble("price"));
				ad.setEmployees(UserDAO.getEmpById(rs.getInt("empid")));

				ads.add(ad); //to store data into array advertisement
			}

			con.close(); //close connection
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ads;
	}

	//WHEN SENIOR
	public static List<Advertisement> getMyAds(int empid){
		
		List<Advertisement> ads = new ArrayList<Advertisement>();

		try {
			con = ConnectionManager.getConnection();

			String sql = "SELECT * FROM advertisements WHERE empid=?";
			ps = con.prepareStatement(sql); //execute query
			ps.setInt(1, empid);
			rs = ps.executeQuery(); 

			//retrieve the data using while loop
			while(rs.next()) {
				Advertisement ad = new Advertisement(); //create object

				ad.setAdsid(rs.getInt("adsid"));
				
				if(rs.getBlob("adspicture") != null) { //if adspicture is not null, setter the data
					
					Blob blob = rs.getBlob("adspicture");
					InputStream inputStream = blob.getBinaryStream();
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					byte[] buffer = new byte[4096];
					int bytesRead = -1;

					while ((bytesRead = inputStream.read(buffer)) != -1) {
						outputStream.write(buffer, 0, bytesRead);                  
					}

					byte[] imageBytes = outputStream.toByteArray();
					String base64Image = Base64.getEncoder().encodeToString(imageBytes);

					inputStream.close();
					outputStream.close();

					ad.setAdspicture(base64Image);
				}
				ad.setAdstitle(rs.getString("adstitle"));
				ad.setAdsdesc(rs.getString("adsdesc"));
				ad.setPrice(rs.getDouble("price"));

				ads.add(ad); //to store data into array advertisement
			}

			con.close(); //close connection
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return ads;
	}
	/* ---------------------------------- END OF READ LIST ADVERTISEMENT ------------------------------------ */
	
	
	/* --------------------------------------- READ SINGLE ADVERTISEMENT ------------------------------------ */
	public static Advertisement getAdById(int adsid) {
		
		Advertisement ad = new Advertisement();

		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			String sql = "SELECT * FROM advertisements a INNER JOIN employees e ON a.empid=e.empid WHERE adsid=?";
			ps = con.prepareStatement(sql); //execute query
			ps.setInt(1, adsid);
			rs = ps.executeQuery(); 

			//retrieve the data using if (only 1 data is retrieved)
			if(rs.next()) {
				ad.setAdsid(rs.getInt("adsid"));
				
				if(rs.getBlob("adspicture") != null) {
					Blob blob = rs.getBlob("adspicture");
					InputStream inputStream = blob.getBinaryStream();
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					byte[] buffer = new byte[4096];
					int bytesRead = -1;

					while ((bytesRead = inputStream.read(buffer)) != -1) {
						outputStream.write(buffer, 0, bytesRead);                  
					}

					byte[] imageBytes = outputStream.toByteArray();
					String base64Image = Base64.getEncoder().encodeToString(imageBytes); 

					inputStream.close();
					outputStream.close();

					ad.setAdspicture(base64Image);
				}
				
				ad.setAdstitle(rs.getString("adstitle"));
				ad.setAdsdesc(rs.getString("adsdesc"));
				ad.setPrice(rs.getDouble("price"));
				ad.setEmployees(UserDAO.getEmpById(rs.getInt("empid")));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		return ad;
	}
	/* ---------------------------------- END OF READ SINGLE ADVERTISEMENT ------------------------------------ */

	
	/* --------------------------------------- ADD ADVERTISEMENT -------------------------------------------- */
	public void addAds(String title, String desc, double adsprice, int emp_id, Part image) {
		adstitle = title;
		adsdesc = desc;
		price = adsprice;
		empid = emp_id;
		adspicture = image;
		
		try {
			con = ConnectionManager.getConnection();
			
			InputStream fis = adspicture.getInputStream();
			
			//INSERT data into database
			String sql = "INSERT INTO advertisements(adstitle, adsdesc, price, adspicture, empid) VALUES(?,?,?,?,?)";
			ps = con.prepareStatement(sql); 

			ps.setString(1, adstitle);
			ps.setString(2, adsdesc);
			ps.setDouble(3, price);
			ps.setBlob(4, fis);
			ps.setInt(5, empid);
			
			ps.executeUpdate(); //execute query

			con.close(); //close connection
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	/* --------------------------------------- END OF ADD ADVERTISEMENT ------------------------------------- */
	
	
	/* --------------------------------------- UPDATE ADVERTISEMENT --------------------------------------- */
	//TITLE, DESC, PRICE
	public void updateAd(Advertisement bean) {
		
		adsid = bean.getAdsid();
		adstitle = bean.getAdstitle();
		adsdesc = bean.getAdsdesc();
		price = bean.getPrice();

		try {			
			//call getConnection() method
			con = ConnectionManager.getConnection();

			//UPDATE advertisement based on adsid
			ps = con.prepareStatement("UPDATE advertisements SET adstitle=?, adsdesc=?, price=? WHERE adsid=?");
			ps.setString(1, adstitle);
			ps.setString(2, adsdesc);
			ps.setDouble(3, price);
			ps.setInt(4, adsid);

			//execute query
			ps.executeUpdate();

			//close connection
			con.close();

		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//PICTURE
	public void updateAdsPic(int ads_id, Part picture) {

		adsid = ads_id;
		adspicture = picture;
		
		try  {			
			//call getConnection() method
			con = ConnectionManager.getConnection();
    		
			ps = con.prepareStatement("UPDATE advertisements SET adspicture=? WHERE adsid=?");
			
			InputStream fis = adspicture.getInputStream();
			
			ps.setBlob(1, fis);
    		ps.setInt(2, adsid);
    		
			//execute query
			ps.executeUpdate();

			//close connection
			con.close();

		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	/* ----------------------------------- END OF UPDATE ADVERTISEMENT ------------------------------------ */
	
	
	/* --------------------------------------- DELETE ADVERTISEMENT --------------------------------------- */
	public void deleteAd(int adsid) {
		try {
			
			con = ConnectionManager.getConnection();

			String sql = "DELETE FROM advertisements WHERE adsid=?";
			ps = con.prepareStatement(sql);
			ps.setInt(1, adsid);
			ps.executeUpdate();

			//close connection
			con.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	/* ------------------------------------ END OF DELETE ADVERTISEMENT ------------------------------------ */

}
