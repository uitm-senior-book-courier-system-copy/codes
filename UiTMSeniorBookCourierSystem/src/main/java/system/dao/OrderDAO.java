package system.dao;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import jakarta.servlet.http.Part;
import system.connection.ConnectionManager;
import system.model.Order;

public class OrderDAO {
	
	static Connection con = null;
	static Statement stmt = null;
	static PreparedStatement ps = null;
	static ResultSet rs = null;
	
	private int orderid, adsid, studid;
	private Date orderdate, mddate; 
	private String typeofdelivery, mdplace, mdtime, status;
	private double totalprice;
	private Part evidencepic;
	
	//CRUD or any other methods
	
	
	/* ------------------------------------- ADMIN STATISTICS ------------------------------------------ */
	//COUNT ADVERTISEMENT THAT HAS BEEN DELIVERED
	public static Order getCountAdsDelivered () {

		Order o = new Order(); //from order.model package --> Order.java

		try {
			con = ConnectionManager.getConnection();

			//3. create statement
			String sql = "SELECT COUNT(adsid) AS countadsdelivered FROM orders WHERE status = 'delivered'";
			ps = con.prepareStatement(sql);

			//4.execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				o.setCountadsdelivered(rs.getInt("countadsdelivered"));
			}
			//close connection
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return o;
	}
	
	//SUM totalprice OF ORDER THAT HAS BEEN DELIVERED
	public static Order getCountTPDelivered () {

		Order o = new Order(); //from order.model package --> Order.java

		try {
			con = ConnectionManager.getConnection();

			//3. create statement
			String sql = "SELECT SUM(totalprice) AS sumtpdelivered FROM orders WHERE status='delivered'";
			ps = con.prepareStatement(sql);

			//4.execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				o.setSumtpdelivered(rs.getInt("sumtpdelivered"));
			}
			//close connection
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return o;
	}
	
	//DETAILS OF STATISTICS
	public static List<Order> getStatOnOrderDelivered () {

		List<Order> orders = new ArrayList<Order>();

		try {
			con = ConnectionManager.getConnection();

			//3. create statement
			String sql = "SELECT * FROM orders o INNER JOIN advertisements a ON a.adsid=o.adsid "
					+ "INNER JOIN employees e ON a.empid=e.empid WHERE o.status='delivered'";
			ps = con.prepareStatement(sql);

			//4.execute query
			rs = ps.executeQuery();
			while(rs.next()) {
				
				Order order = new Order(); //create object
				
				order.setAds(AdvertisementDAO.getAdById(rs.getInt("adsid")));
				order.setTotalprice(rs.getDouble("totalprice"));
				
				orders.add(order);
			}
			
			//close connection
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return orders;
	}
	/* ------------------------------------- END OF ADMIN STATISTICS ----------------------------------- */
	
	
	/* ---------------------------------- READ LIST ORDER (SENIOR) ------------------------------------------- */
	//PENDING AND REJECTED
	public static List<Order> getPendingRejectedShipment(int empid, String status) {
		List<Order> orders = new ArrayList<Order>();

		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			//create statement
			stmt = con.createStatement();

			//SELECT ALL orders, advertisements and students details
			String sql= "";
			
			if(status.equalsIgnoreCase("pending")) {
				sql = "SELECT * FROM students s "
						+ "INNER JOIN orders o ON o.studid = s.studid "
						+ "INNER JOIN advertisements a ON a.adsid = o.adsid "
						+ "WHERE o.status ='pending' AND a.empid=?";
			}
			
			if(status.equalsIgnoreCase("rejected")) {
				sql = "SELECT * FROM students s "
						+ "INNER JOIN orders o ON o.studid = s.studid "
						+ "INNER JOIN advertisements a ON a.adsid = o.adsid "
						+ "WHERE o.status ='rejected' AND a.empid=?";
			}
			
			//execute query
			ps = con.prepareStatement(sql);
			
			ps.setInt(1, empid);
			
			//execute query
			rs = ps.executeQuery(); 

			//retrieve the data using while loop
			while(rs.next()) {
				Order order = new Order(); //create object

				//Order Details
				order.setOrderid(rs.getInt("orderid")); //store data in adsid TEMPORARILY
				order.setOrderdate(rs.getDate("orderdate"));
				order.setTypeofdelivery(rs.getString("typeofdelivery"));
				order.setStatus(rs.getString("status"));
				order.setMddate(rs.getDate("mddate"));
				order.setMdtime(rs.getString("mdtime"));
				order.setMdplace(rs.getString("mdplace"));
				order.setTotalprice(rs.getDouble("totalprice"));

				//Advertisement Details
				order.setAds(AdvertisementDAO.getAdById(rs.getInt("adsid")));

				//Student Details
				order.setStudents(UserDAO.getStudById(rs.getInt("studid")));

				orders.add(order); //to store data into array orders
			}

			//close connection
			con.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return orders;
	}
	
	//ACCEPTED, PREPARED, DELIVERED
	public static List<Order> getMyShipments(int empid) {
		List<Order> orders = new ArrayList<Order>();

		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			//create statement
			stmt = con.createStatement();

			String sql = "SELECT * FROM students s "
					+ "INNER JOIN orders o ON o.studid = s.studid "
					+ "INNER JOIN advertisements a ON a.adsid = o.adsid "
					+ "WHERE (o.status!='pending' AND o.status!='rejected') AND a.empid=?";
			
			//execute query
			ps = con.prepareStatement(sql);
			
			ps.setInt(1, empid);
			
			//execute query
			rs = ps.executeQuery(); 

			//retrieve the data using while loop
			while(rs.next()) {
				Order order = new Order(); //create object

				//Order Details
				order.setOrderid(rs.getInt("orderid")); //store data in adsid TEMPORARILY
				order.setOrderdate(rs.getDate("orderdate"));
				order.setTypeofdelivery(rs.getString("typeofdelivery"));
				order.setStatus(rs.getString("status"));
				order.setMddate(rs.getDate("mddate"));
				order.setMdtime(rs.getString("mdtime"));
				order.setMdplace(rs.getString("mdplace"));
				order.setTotalprice(rs.getDouble("totalprice"));

				//Advertisement Details
				order.setAds(AdvertisementDAO.getAdById(rs.getInt("adsid")));

				//Student Details
				order.setStudents(UserDAO.getStudById(rs.getInt("studid")));

				orders.add(order); //to store data into array orders
			}

			//close connection
			con.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return orders;
	}
	/* ---------------------------------- END OF READ LIST ORDER (SENIOR) ----------------------------------- */
	
	
	/* ---------------------------------- UPDATE STATUS ----------------------------------------------- */
	//['pending' -> 'accepted']
	public void acceptRejectOrder(int orderid, String status) {
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			String sql = "";
			if(status.equalsIgnoreCase("accepted")) //UPDATE order status ['pending' -> 'accepted']
				sql = "UPDATE orders SET status='accepted' WHERE orderid=?";
			
			if(status.equalsIgnoreCase("rejected")) //UPDATE order status ['pending' -> 'rejected']
				sql = "UPDATE orders SET status='rejected' WHERE orderid=?";
			
			ps = con.prepareStatement(sql);

			ps.setInt(1, orderid);

			//execute query
			ps.executeUpdate();

			//close connection
			con.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	//['prepared' -> 'delivered']
	public void updateStatus(int orderid, String status) {
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			String sql = "";
			if(status.equalsIgnoreCase("accepted")) { //UPDATE order status ['accepted' -> 'prepared']
				sql = "UPDATE orders SET status='prepared' WHERE orderid=?";
			}
			
			if(status.equalsIgnoreCase("prepared")) { //UPDATE order status ['prepared' -> 'delivered']
				sql = "UPDATE orders SET status='delivered' WHERE orderid=?";
			}
			//String sql = "UPDATE orders SET status='delivered' WHERE orderid=?";
			ps = con.prepareStatement(sql);

			ps.setInt(1, orderid);

			//execute query
			ps.executeUpdate();

			//close connection
			con.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	/* ---------------------------------- END OF UPDATE STATUS ----------------------------------------------- */
	
	
	/* ---------------------------------- UPDATE PARCEL EVIDENCE ----------------------------------------- */
	public void addEvidencepic(int order_id, Part picture) {

		orderid = order_id;
		evidencepic = picture;
		
		try  {			
			//call getConnection() method
			con = ConnectionManager.getConnection();
    		
			ps = con.prepareStatement("UPDATE orders SET evidencepic=? WHERE orderid=?");
			
			InputStream fis = evidencepic.getInputStream();
			
			ps.setBlob(1, fis);
    		ps.setInt(2, orderid);
    		
			//execute query
			ps.executeUpdate();

			//close connection
			con.close();

		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
	/* ---------------------------------- END OF UPDATE PARCEL EVIDENCE ------------------------------------- */
	
	
	/* ---------------------------------- READ LIST ORDER (STUDENT) ------------------------------------- */
	public static List<Order> getAllStudOrders(int studid) {
		
		List<Order> orders = new ArrayList<Order>();
		
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();
			//3. create statement
			stmt = con.createStatement();
			String sql = "SELECT * FROM orders WHERE studid=?";
			//4.execute query
			ps = con.prepareStatement(sql);

			ps.setInt(1, studid);

			rs = ps.executeQuery(); 
			while(rs.next()) {
				
				Order order = new Order();
				
				order.setOrderid(rs.getInt("orderid"));
				order.setOrderdate(rs.getDate("orderdate"));
				order.setStatus(rs.getString("status"));
				order.setAds(AdvertisementDAO.getAdById(rs.getInt("adsid")));

				orders.add(order);
			}
			//5.close connection
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}

		return orders;
	}
	/* ---------------------------------- END OF READ LIST ORDER (STUDENT) --------------------------------- */
	
	
	/* ---------------------------------- READ SINGLE ORDER (STUDENT) ------------------------------------- */
	//CHECK ORDERID IF IT IS UNDER SAME STUDID BEFORE CAN VIEW THE SHIPMENT
	public static boolean checkOrderid (int orderid, int studid) {

		boolean found = false;

		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			//3. create statement
			String sql = "SELECT * FROM orders WHERE orderid=? AND studid=?";
			ps = con.prepareStatement(sql);
			ps.setInt(1, orderid);
			ps.setInt(2, studid);

			//4.execute query
			rs = ps.executeQuery();
			if(rs.next()) {
				found = true;
			}
			//close connection
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}

		return found;
	}
	
	//READ STUDENT SHIPMENT
	public static Order getOrderShipment(int orderid) {
		
		Order order = new Order();

		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();

			String sql = "SELECT * FROM orders o INNER JOIN advertisements a ON o.adsid=a.adsid "
					+ "INNER JOIN students s ON o.studid=s.studid WHERE o.orderid=?";
			
			//execute query
			ps = con.prepareStatement(sql);

			ps.setInt(1, orderid);

			rs = ps.executeQuery(); 

			//retrieve the data using if (only 1 data is retrieved)
			if(rs.next()) {
				
				if(rs.getBlob("evidencepic") != null) {
					Blob blob = rs.getBlob("evidencepic");
					InputStream inputStream = blob.getBinaryStream();
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					byte[] buffer = new byte[4096];
					int bytesRead = -1;
	
					while ((bytesRead = inputStream.read(buffer)) != -1) {
						outputStream.write(buffer, 0, bytesRead);                  
					}
	
					byte[] imageBytes = outputStream.toByteArray();
					String base64Image = Base64.getEncoder().encodeToString(imageBytes); 
	
					inputStream.close();
					outputStream.close();
	
					order.setEvidencepic(base64Image);
				}
				order.setOrderid(rs.getInt("orderid"));
				order.setStudents(UserDAO.getStudById(rs.getInt("studid")));
				order.setAds(AdvertisementDAO.getAdById(rs.getInt("adsid")));
				order.setStatus(rs.getString("status"));
				order.setTotalprice(rs.getDouble("totalprice"));
				order.setMddate(rs.getDate("mddate"));
				order.setMdplace(rs.getString("mdplace"));
				order.setMdtime(rs.getString("mdtime"));
				order.setTypeofdelivery(rs.getString("typeofdelivery"));
			}
			//close connection
			con.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		return order;
	}
	/* ---------------------------------- END OF READ SINGLE ORDER (STUDENT) --------------------------------- */

	
	/* ---------------------------------- CHECKOUT ORDER (ADD) --------------------------------- */
	public void AddOrder (Order bean) {

		//retrieve from controller
		orderdate = bean.getOrderdate();
		typeofdelivery = bean.getTypeofdelivery();
		mddate = bean.getMddate();
		mdplace = bean.getMdplace();
		mdtime = bean.getMdtime();
		status = "pending";
		totalprice = bean.getTotalprice();
		studid = bean.getStudid();
		adsid = bean.getAdsid();
		
		 //convert java date to sql date
        java.sql.Date odate = new java.sql.Date(orderdate.getTime());
        java.sql.Date mdate = new java.sql.Date(mddate.getTime());
		
		try {
			//call getConnection() method from ConnectionManager class
			con = ConnectionManager.getConnection();
			
			//3. create statement
			String sql = "INSERT INTO orders(orderdate,typeofdelivery,mddate,mdplace,mdtime,status,totalprice,studid,adsid)"
					+ "VALUES(?,?,?,?,?,?,?,?,?)"; //? for marker
			ps = con.prepareStatement(sql);
			ps.setDate(1, odate);
			ps.setString(2, typeofdelivery);
			ps.setDate(3, mdate);
			ps.setString(4, mdplace);
			ps.setString(5, mdtime);
			ps.setString(6, status);
			ps.setDouble(7, totalprice);
			ps.setInt(8, studid);
			ps.setInt(9, adsid);
			
			//4. execute query
			ps.executeUpdate();

			//5.close connection
			con.close();

		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	/* ---------------------------------- END OF CHECKOUT ORDER (ADD) --------------------------------- */
}