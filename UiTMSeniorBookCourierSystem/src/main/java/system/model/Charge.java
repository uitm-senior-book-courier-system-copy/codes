package system.model;

import java.util.Date;

public class Charge {
	
	private int chargeid; //PK
	private double chargeonorder;
	private String chargeontype;
	private Date datechanged = new Date();
	private int empid; //FK
	private User admin; 

	public Charge() {}
	
	//chargeid
	public int getChargeid() {
		return chargeid;
	}
	public void setChargeid(int chargeid) {
		this.chargeid = chargeid;
	}
	
	//chargeonorder
	public double getChargeonorder() {
		return chargeonorder;
	}
	public void setChargeonorder(double chargeonorder) {
		this.chargeonorder = chargeonorder;
	}
	
	//chargeontype
	public String getChargeontype() {
		return chargeontype;
	}
	public void setChargeontype(String chargeontype) {
		this.chargeontype = chargeontype;
	}
	
	//datechanged
	public Date getDatechanged() {
		return datechanged;
	}
	public void setDatechanged(Date datechanged) {
		this.datechanged = datechanged;
	}
	
	//empid
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	
	//FK
	public User getAdmin() {
		return admin;
	}

	public void setAdmin(User admin) {
		this.admin = admin;
	}

}
