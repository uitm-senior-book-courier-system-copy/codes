package system.model;

import java.util.Date;

public class Order {
	
	private int orderid; //PK
	private Date orderdate = new Date(); 
	private String typeofdelivery;
	private Date mddate = new Date();
	private String mdplace;
	private String mdtime;
	private String status;
	private double totalprice;
	private String evidencepic;
	private int studid; //FK
	private int adsid; //FK
	private User students;
	private Advertisement ads;
	
	//----------------------------------------
	//FOR STATISTIC
	private int countadsdelivered;
	private double sumtpdelivered;

	public Order() {}
	
	//orderid
	public int getOrderid() {
		return orderid;
	}
	public void setOrderid(int orderid) {
		this.orderid = orderid;
	}
	
	//orderdate
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}
	
	//typeofdelivery
	public String getTypeofdelivery() {
		return typeofdelivery;
	}
	public void setTypeofdelivery(String typeofdelivery) {
		this.typeofdelivery = typeofdelivery;
	}
	
	//mddate
	public Date getMddate() {
		return mddate;
	}
	public void setMddate(Date mddate) {
		this.mddate = mddate;
	}
	
	//mdplace
	public String getMdplace() {
		return mdplace;
	}
	public void setMdplace(String mdplace) {
		this.mdplace = mdplace;
	}
	
	//mdtime
	public String getMdtime() {
		return mdtime;
	}
	public void setMdtime(String mdtime) {
		this.mdtime = mdtime;
	}
	
	//status
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	//totalprice
	public double getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(double totalprice) {
		this.totalprice = totalprice;
	}
	
	public String getEvidencepic() {
		return evidencepic;
	}

	public void setEvidencepic(String evidencepic) {
		this.evidencepic = evidencepic;
	}
	
	//adsid
	public int getAdsid() {
		return adsid;
	}
	public void setAdsid(int adsid) {
		this.adsid = adsid;
	}
	
	//studid
	public int getStudid() {
		return studid;
	}
	public void setStudid(int studid) {
		this.studid = studid;
	}
	
	//FK
	public User getStudents() {
		return students;
	}

	public void setStudents(User students) {
		this.students = students;
	}
	
	//FK
	public Advertisement getAds() {
		return ads;
	}

	public void setAds(Advertisement ads) {
		this.ads = ads;
	}
	
	//FOR STATISTIC
	public int getCountadsdelivered() {
		return countadsdelivered;
	}

	public void setCountadsdelivered(int countadsdelivered) {
		this.countadsdelivered = countadsdelivered;
	}
	
	//FOR STATISTIC
	public double getSumtpdelivered() {
		return sumtpdelivered;
	}

	public void setSumtpdelivered(double sumtpdelivered) {
		this.sumtpdelivered = sumtpdelivered;
	}

}
