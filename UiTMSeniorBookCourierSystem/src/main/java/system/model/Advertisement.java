package system.model;

public class Advertisement {
	
	private int adsid; //PK
	private String adstitle;
	private String adsdesc;
	private double price;
	private String adspicture;
	private int empid; //FK
	private User employees;

	public Advertisement() {}
	
	//adsid
	public int getAdsid() {
		return adsid;
	}
	public void setAdsid(int adsid) {
		this.adsid = adsid;
	}
	
	//adstitle
	public String getAdstitle() {
		return adstitle;
	}
	public void setAdstitle(String adstitle) {
		this.adstitle = adstitle;
	}
	
	//adsdesc
	public String getAdsdesc() {
		return adsdesc;
	}
	public void setAdsdesc(String adsdesc) {
		this.adsdesc = adsdesc;
	}
	
	//price
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	//adspicture
	public String getAdspicture() {
		return adspicture;
	}
	public void setAdspicture(String adspicture) {
		this.adspicture = adspicture;
	}
	
	//empid
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	
	//FK
	public User getEmployees() {
		return employees;
	}

	public void setEmployees(User employees) {
		this.employees = employees;
	}


}
