package system.model;

public class User {
	
	//EMPLOYEES (senior/admin)
	private int empid;
	private String emppassword;
	private String username;
	private String name;
	private String empgender;
	private String empphonenum;
	private String empemail;
	private String role;
	private boolean Empvalid;

	//----------------------------------------
	//STUDENTS (UiTM students)
	private int studid;
	private String matricnum;
	private String studpassword;
	private String firstname;
	private String lastname;
	private String studgender;
	private String studphonenum;
	private String studemail;
	private String collegehousenum;
	private String collegeblock;

	public User() {}

	//EMPLOYEES (empid)
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}

	//EMPLOYEES (emppassword)
	public String getEmppassword() {
		return emppassword;
	}
	public void setEmppassword(String emppassword) {
		this.emppassword = emppassword;
	}

	//EMPLOYEES (username)
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	//EMPLOYEES (name)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	//EMPLOYEES (empgender)
	public String getEmpgender() {
		return empgender;
	}
	public void setEmpgender(String empgender) {
		this.empgender = empgender;
	}

	//EMPLOYEES (empphonenum)
	public String getEmpphonenum() {
		return empphonenum;
	}
	public void setEmpphonenum(String empphonenum) {
		this.empphonenum = empphonenum;
	}

	//EMPLOYEES (empemail)
	public String getEmpemail() {
		return empemail;
	}
	public void setEmpemail(String empemail) {
		this.empemail = empemail;
	}

	//EMPLOYEES (role)
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	//EMPLOYEES VALID
	public boolean isEmpvalid() {
		return Empvalid;
	}

	public void setEmpvalid(boolean empvalid) {
		Empvalid = empvalid;
	}


	//----------------------------------------
	//STUDENTS (studid)
	public int getStudid() {
		return studid;
	}
	public void setStudid(int studid) {
		this.studid = studid;
	}

	//STUDENTS (matricnum)
	public String getMatricnum() {
		return matricnum;
	}
	public void setMatricnum(String matricnum) {
		this.matricnum = matricnum;
	}

	//STUDENTS (studpassword)
	public String getStudpassword() {
		return studpassword;
	}
	public void setStudpassword(String studpassword) {
		this.studpassword = studpassword;
	}

	//STUDENTS (firstname)
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	//STUDENTS (lastname)
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	//STUDENTS (studgender)
	public String getStudgender() {
		return studgender;
	}
	public void setStudgender(String studgender) {
		this.studgender = studgender;
	}

	//STUDENTS (studphonenum)
	public String getStudphonenum() {
		return studphonenum;
	}
	public void setStudphonenum(String studphonenum) {
		this.studphonenum = studphonenum;
	}

	//STUDENTS (studemail)
	public String getStudemail() {
		return studemail;
	}
	public void setStudemail(String studemail) {
		this.studemail = studemail;
	}

	//STUDENTS (collegehousenum)
	public String getCollegehousenum() {
		return collegehousenum;
	}
	public void setCollegehousenum(String collegehousenum) {
		this.collegehousenum = collegehousenum;
	}

	//STUDENTS (collegeblock)
	public String getCollegeblock() {
		return collegeblock;
	}
	public void setCollegeblock(String collegeblock) {
		this.collegeblock = collegeblock;
	}

}
