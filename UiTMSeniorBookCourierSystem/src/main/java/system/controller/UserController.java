package system.controller;

import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
//import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import system.model.User;
import system.dao.OrderDAO;
import system.dao.UserDAO;

/**
 * Servlet implementation class UserController
 */
public class UserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userdao;
	private String action, forward;
	private HttpSession session;
	private int empid, studid;
	
	//EMPLOYEES (SENIOR, ADMIN)
	private static String adminDashboard ="AD_B_dashboard.jsp";
	private static String seniorDashboard ="SE_B_dashboard.jsp";
	private static String seniorList ="AD_C1_seniorlist.jsp";
	private static String seniorView ="AD_C2_seniorview.jsp";
	private static String seniorAdd ="AD_C3_senioradd.jsp";
	private static String seniorProfile ="SE_C_profileEmp.jsp";
	private static String adminProfile ="AD_E_profileEmp.jsp";
	
	//STUDENTS
	private static String studentRegister ="ST_A_register.jsp";
	private static String studentDashboard ="ST_B_dashboard.jsp";
	private static String studentProfile ="ST_C_profileStud.jsp";
	
	private static String Aindex ="A_index.jsp";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController() {
        super();
        userdao = new UserDAO();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		action = request.getParameter("action");
		
		
		/* ------------------------------- READ LIST, SINGLE & DELETE SENIOR ----------------------------- */
		//READ LIST SENIOR
		if(action.equalsIgnoreCase("seniorlist")) {
			forward = seniorList;
			
			request.setAttribute("seniors", UserDAO.getAllSeniors());
		}
		
		//READ SINGLE SENIOR
		if(action.equalsIgnoreCase("seniorview")) {
			forward = seniorView;
			
			empid = Integer.parseInt(request.getParameter("empid"));
			
			request.setAttribute("senior", UserDAO.getEmpById(empid));
		}
		
		//DELETE SENIOR
		if(action.equalsIgnoreCase("seniordelete")) {
			forward = seniorList;
			
			empid = Integer.parseInt(request.getParameter("empid"));
			
			userdao.deleteSenior(empid);
			
			request.setAttribute("seniors", UserDAO.getAllSeniors());
			request.setAttribute("result","Successfully deleted");
		}
		/* ---------------------------- END OF READ LIST, SINGLE & DELETE SENIOR -------------------------- */
		
		
		/* --------------------------------------- PROFILE ----------------------------------------- */
		//STUDENT
		if(action.equalsIgnoreCase("studentprofile")) {

			session = request.getSession(true);
			studid = (Integer) session.getAttribute("id");
			
			forward = studentProfile;
			request.setAttribute("stud", UserDAO.getStudById(studid));
		}
		
		//EMPLOYEES (SENIOR / ADMIN)
		if(action.equalsIgnoreCase("empprofile")) {

			session = request.getSession(true);
			empid = (Integer) session.getAttribute("id");
			String roleS = (String) session.getAttribute("role");
			
			if (roleS.equalsIgnoreCase("senior"))
				forward = seniorProfile;
			if(roleS.equalsIgnoreCase("admin"))
				forward = adminProfile;
			
			request.setAttribute("emp", UserDAO.getEmpById(empid));
		}
		/* ---------------------------------------- END OF PROFILE -------------------------------------- */
		
		
		/* -------------------------------------- LOGOUT -------------------------------------------- */
		if(action.equalsIgnoreCase("logout")) {
			try {
				session = request.getSession(true); //get the current session
				
				session.setAttribute("id", null);
				session.setAttribute("name", null);
				session.setAttribute("role", null);
				session.setAttribute("studgender", null);
				
				session.invalidate(); //destroy the session
				
				if(session == null || !request.isRequestedSessionIdValid()) { // user is not authorized
					forward = Aindex;
					request.setAttribute("result","User has successfully logout from the system");
				}
				
			}catch (Throwable ex) {
				System.out.println(ex);
			}
		}
		/* -------------------------------------- END OF LOGOUT -------------------------------------------- */
		
		
		/* -------------------------------- FORWARD THE REQUEST ------------------------------------------ */
		RequestDispatcher req = request.getRequestDispatcher(forward);
		req.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		action = request.getParameter("action");
		
		
		/* ----------------------------------------- ADD USER -------------------------------------------- */
		//STUDENT (REGISTER)
		if(action.equalsIgnoreCase("register")) {
			
			User user = new User();

			user.setMatricnum(request.getParameter("matricnum"));
			user.setStudpassword(request.getParameter("studpassword"));
			user.setFirstname(request.getParameter("firstname"));
			user.setLastname(request.getParameter("lastname"));
			user.setStudgender(request.getParameter("studgender"));
			user.setStudphonenum(request.getParameter("studphonenum"));
			user.setStudemail(request.getParameter("studemail"));
			user.setCollegehousenum(request.getParameter("collegehousenum"));
			user.setCollegeblock(request.getParameter("collegeblock"));

			boolean similarMtricNum = userdao.checkStudentReg(user); //check matric num

			if(similarMtricNum == false) {
				forward = studentRegister;
				userdao.registerStudent(user); //invoke method in UserDAO ; pass object
				request.setAttribute("result","You have successfully registered.");
			}
			else {
				forward = studentRegister;
				request.setAttribute("result","You already registered in the system with "
						+ "matric num: " +request.getParameter("matricnum"));
			}
		}
		
		//SENIOR
		if(action.equalsIgnoreCase("senioradd")) {

			User user = new User();

			user.setEmppassword(request.getParameter("emppassword"));
			user.setUsername(request.getParameter("username"));
			user.setName(request.getParameter("name"));
			user.setEmpgender(request.getParameter("empgender"));
			user.setEmpphonenum(request.getParameter("empphonenum"));
			user.setEmpemail(request.getParameter("empemail"));
			user.setRole(request.getParameter("role"));

			boolean similarUsername = userdao.checkSeniorAdd(request.getParameter("username"));

			if(similarUsername == false) {
				forward = seniorList;
				userdao.AddSenior(user); //invoke method in UserDAO ; pass object
				request.setAttribute("seniors", UserDAO.getAllSeniors());
				request.setAttribute("result","Successfully registered");
			}
			else {
				forward = seniorAdd;
				request.setAttribute("result","Please enter different username. "
						+ "There a user that use the same username as " + request.getParameter("username"));
			}
		}
		/* --------------------------------------- END OF ADD USER ---------------------------------------- */
		
		
		/* ------------------------------------------- LOGIN -------------------------------------------- */
		//STUDENT
		if(action.equalsIgnoreCase("studlogin")) {

			try {
				User user = new User();
				
				user.setMatricnum(request.getParameter("matricnum"));
				user.setStudpassword(request.getParameter("studpassword"));
				
				user = UserDAO.studlogin(user);
				
				//set user session
				if(user.getFirstname() != null) {
					session = request.getSession(true);
					session.setAttribute("id", user.getStudid());
					session.setAttribute("name", user.getFirstname());
					session.setAttribute("role", "student");
					session.setAttribute("studgender", user.getStudgender());
					
					forward = studentDashboard;
				}
				else {
					forward = Aindex;
					request.setAttribute("result","Error! information is not existing");
				}
				
			}catch (Throwable ex) {
				ex.printStackTrace();
			}	
		}
		
		//EMPLOYEES (SENIOR / ADMIN)
		if(action.equalsIgnoreCase("emplogin")) {
			
			try {
				User user = new User();
				
				user.setUsername(request.getParameter("username"));
				user.setEmppassword(request.getParameter("emppassword"));
				
				user = UserDAO.emplogin(user);
				
				//set user session
				if(user.isEmpvalid()) {
					session = request.getSession(true);
					session.setAttribute("id", user.getEmpid());
					session.setAttribute("name", user.getName());
					session.setAttribute("role", user.getRole());
					
					if(user.getRole().equalsIgnoreCase("admin")) {
						forward = adminDashboard;
						request.setAttribute("adsDelivered", OrderDAO.getCountAdsDelivered());
						request.setAttribute("totalpriceDelivered", OrderDAO.getCountTPDelivered());
						request.setAttribute("stats", OrderDAO.getStatOnOrderDelivered());
					}
					else {
						forward = seniorDashboard;
					}
				}
				else {
					forward = Aindex;
					request.setAttribute("result","Error! information is not existing");
				}
				
			}catch (Throwable ex) {
				ex.printStackTrace();
			}
		}
		/* ------------------------------------------ END OF LOGIN ----------------------------------------- */

		
		/* ---------------------------------- UPDATE PROFILE & PASSWORD -------------------------------------- */
		//STUDENT
		if(action.equalsIgnoreCase("updateSprofile"))
		{
			User stud = new User();
			
			session = request.getSession(true);
			int studid = (Integer) session.getAttribute("id");
			String roleStud = (String) session.getAttribute("role");
			
			String studpassword = request.getParameter("studpassword");
			
			stud.setStudid(studid);
			
			if(studpassword != null) {
				
				boolean similarPsPrevious = userdao.checkPsPre(studpassword, roleStud, studid);

				if(similarPsPrevious == true) { //check password with previous
					request.setAttribute("getAlert", "same password");
				}
				else {
					stud.setStudpassword(studpassword);
					userdao.updatePassword(stud, roleStud); //Update password
					request.setAttribute("getAlert", "UpdatePs Success");
				}
			}
			else {
				stud.setMatricnum(request.getParameter("matricnum"));
				stud.setFirstname(request.getParameter("firstname"));
				stud.setLastname(request.getParameter("lastname"));
				stud.setStudgender(request.getParameter("studgender"));
				stud.setStudphonenum(request.getParameter("studphonenum"));
				stud.setStudemail(request.getParameter("studemail"));
				stud.setCollegehousenum(request.getParameter("collegehousenum"));
				stud.setCollegeblock(request.getParameter("collegeblock"));
				
				userdao.updateSTProfile(stud); //update profile
				
				session = request.getSession(true); //re-session back the name
				session.setAttribute("name", stud.getFirstname());
				session.setAttribute("studgender", stud.getStudgender());
				
				request.setAttribute("getAlert", "Update Success");
			}
			
			forward = studentProfile;
			request.setAttribute("stud", UserDAO.getStudById(studid));
		}
		
		//EMPLOYEES (ADMIN / SENIOR)
		if(action.equalsIgnoreCase("updateEprofile"))
		{
			User emp = new User();

			session = request.getSession(true);
			int empid = (Integer) session.getAttribute("id");
			String roleEmp = (String) session.getAttribute("role");

			String emppassword = request.getParameter("emppassword");

			emp.setEmpid(empid);

			if(emppassword != null) {
				
				boolean similarPsPrevious = userdao.checkPsPre(emppassword, roleEmp, empid);
				
				if(similarPsPrevious == true) { //check password with previous
					request.setAttribute("getAlert", "same password");
				}
				else {
					emp.setEmppassword(emppassword);
					userdao.updatePassword(emp, roleEmp); //Update password
					request.setAttribute("getAlert", "UpdatePs Success");
				}
			}
			else {
				emp.setUsername(request.getParameter("username"));
				emp.setName(request.getParameter("name"));
				emp.setEmpgender(request.getParameter("empgender"));
				emp.setEmpphonenum(request.getParameter("empphonenum"));
				emp.setEmpemail(request.getParameter("empemail"));

				userdao.updateEProfile(emp); //update profile
				
				session = request.getSession(true); //re-session back the name
				session.setAttribute("name", emp.getName());
				
				request.setAttribute("getAlert", "Update Success");
			}

			if(roleEmp.equalsIgnoreCase("senior"))
				forward = seniorProfile;
			if(roleEmp.equalsIgnoreCase("admin"))
				forward = adminProfile;
			
			request.setAttribute("emp", UserDAO.getEmpById(empid));
		}
		/* ---------------------------------- END OF UPDATE PROFILE & PASSWORD ------------------------------- */
		
		
		/* -------------------------------- FORWARD THE REQUEST ------------------------------------------ */
		RequestDispatcher req = request.getRequestDispatcher(forward);
		req.forward(request, response);
	}

}
