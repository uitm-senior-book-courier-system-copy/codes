package system.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
//import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import system.dao.AdvertisementDAO;
import system.dao.ChargeDAO;
import system.model.Advertisement;

import java.io.IOException;

/**
 * Servlet implementation class AdvertisementController
 */
@MultipartConfig(maxFileSize = 16777215) 
public class AdvertisementController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AdvertisementDAO adsdao;
	private HttpSession session;
    
    private static String ADMIN_LIST = "AD_F1_viewAdsList.jsp";
    private static String ADMIN_VIEW ="AD_F2_viewDetailsAdsList.jsp"; 
    
    private static String SENIOR_LIST ="SE_D1_viewAdsList.jsp";
    private static String SENIOR_VIEW ="SE_D2_viewDetailsAdsList.jsp";
    private static String SENIOR_UPDATE ="SE_D4_editAdvertisement.jsp";
    
    private static String STUDENT_LIST = "ST_D1_viewAdsList.jsp";
    private static String STUDENT_VIEW ="ST_D2_makeOrder.jsp"; 
    private static String STUDENT_ORDER ="ST_D3_orderForm.jsp"; 
    
    private String action, forward, role;
    private int adsid, empid;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdvertisementController() {
        super();
        adsdao = new AdvertisementDAO();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Get the session
		session = request.getSession(true);
	    role = (String) session.getAttribute("role");
	    empid = (Integer) session.getAttribute("id");
	    
		action = request.getParameter("action"); //receive what is the action
		
		
		/* ------------------------------------ READ LIST ADVERTISEMENT ----------------------------------- */
		if(action.equalsIgnoreCase("adslist"))
		{
			if (role.equalsIgnoreCase("admin")) {
				forward = ADMIN_LIST; //specify where to forward
				request.setAttribute("ads", AdvertisementDAO.getAllAds());
			}

			if (role.equalsIgnoreCase("senior")) {
				//session = request.getSession(true);
			    //empid = (Integer) session.getAttribute("id");
			    
				forward = SENIOR_LIST;
			    
				request.setAttribute("ads", AdvertisementDAO.getMyAds(empid));
			}
			
			if (role.equalsIgnoreCase("student")) {
				forward = STUDENT_LIST; //specify where to forward
				request.setAttribute("ads", AdvertisementDAO.getAllAds());
			}
		}
		/* ------------------------------- END OF READ LIST ADVERTISEMENT --------------------------------- */
		
		
		/* ------------------------------------ READ SINGLE ADVERTISEMENT -------------------------------- */
		if(action.equalsIgnoreCase("view"))	
		{
			//get adsid from the request
			adsid = Integer.parseInt(request.getParameter("adsid"));
			
			//set attribute to a servlet request
			request.setAttribute("ad", AdvertisementDAO.getAdById(adsid));
			
			if (role.equalsIgnoreCase("admin"))
				forward = ADMIN_VIEW; 
			if (role.equalsIgnoreCase("senior"))
				forward = SENIOR_VIEW; 
			if (role.equalsIgnoreCase("student"))
				forward = STUDENT_VIEW;
			
		}
		/* --------------------------------- END OF READ SINGLE ADVERTISEMENT ------------------------------- */
		
		
		/* ------------------------------------ DELETE ADVERTISEMENT --------------------------------------- */
		//DELETE
		if(action.equalsIgnoreCase("delete"))
		{	
			//get empid from session
			session = request.getSession(true);
		    empid = (Integer) session.getAttribute("id");
			
			forward = SENIOR_LIST;
			
			adsid = Integer.parseInt(request.getParameter("adsid")); //get adsid from the request
			
			adsdao.deleteAd(adsid); //invoke method deleteAd() in AdvertisementDAO
			
			//set attribute to a servlet request 
			request.setAttribute("getAlert", "Delete Success");
			request.setAttribute("ads", AdvertisementDAO.getMyAds(empid));
		}
		
		//BEFORE CAN UPDATE, THE FORM DISPLAY THE DETAILS OF SELECTED ADVERTISEMENT
		if(action.equalsIgnoreCase("update")) //pass data to the form before edit
		{
			forward = SENIOR_UPDATE;

			adsid = Integer.parseInt(request.getParameter("adsid"));

			request.setAttribute("ad", AdvertisementDAO.getAdById(adsid)); 
		}
		/* ---------------------------------- END OF DELETE ADVERTISEMENT ---------------------------------- */
		
		
		/* -------------------------------------- STUDENT ORDER FORM ------------------------------------- */
		if(action.equalsIgnoreCase("order"))	
		{
			//get adsid from the request
			adsid = Integer.parseInt(request.getParameter("adsid"));

			request.setAttribute("ad", AdvertisementDAO.getAdById(adsid));
			request.setAttribute("charge", ChargeDAO.getCharge());
			
			forward = STUDENT_ORDER;
		}
		/* -------------------------------- END OF STUDENT ORDER FORM ------------------------------------- */
		
		
		/* -------------------------------- FORWARD THE REQUEST ------------------------------------------ */
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 *
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Get the Session
		session = request.getSession(true);
		empid = (Integer) session.getAttribute("id"); 
		
		action = request.getParameter("action"); //receive what is the action
		
		
		/* -------------------------------- ADD ADVERTISEMENT ------------------------------------------ */
		if(action.equalsIgnoreCase("addads")) 
		{		
			String adstitle = request.getParameter("adstitle");
			String adsdesc = request.getParameter("adsdesc");
			double adsprice = Double.parseDouble(request.getParameter("price"));
			
			Part adspicture = request.getPart("adspicture");
			
			adsdao.addAds(adstitle, adsdesc, adsprice, empid, adspicture);
			
			forward = SENIOR_LIST;

			request.setAttribute("ads", AdvertisementDAO.getMyAds(empid));
			request.setAttribute("getAlert", "Add Success");
		}
		/* -------------------------------- END OF ADD ADVERTISEMENT ----------------------------------- */
		
		
		/* -------------------------------- UPDATE ADVERTISEMENT --------------------------------------- */
		//UPDATE TITLE, DESC & PRICE
		if(action.equalsIgnoreCase("editads"))
		{
			Advertisement ad = new Advertisement();

			adsid = Integer.parseInt(request.getParameter("adsid"));
			String adstitle = request.getParameter("adstitle");
			String adsdesc = request.getParameter("adsdesc");
			double adsprice = Double.parseDouble(request.getParameter("price"));
			
			ad.setAdsid(adsid);
			ad.setAdstitle(adstitle);
			ad.setAdsdesc(adsdesc);
			ad.setPrice(adsprice);

			adsdao.updateAd(ad); //invoke method updateAd() in AdvertisementDAO
			
			forward = SENIOR_VIEW;

			request.setAttribute("ad", AdvertisementDAO.getAdById(adsid));
			request.setAttribute("getAlert", "Update Success");
		} 
		
		//UPDATE PICTURE
		if(action.equalsIgnoreCase("addadspic"))
		{
	    	forward = SENIOR_VIEW;
			
	    	adsid = Integer.parseInt(request.getParameter("adsid"));
			Part adspicture = request.getPart("adspicture");
			
			adsdao.updateAdsPic(adsid, adspicture); //invoke method updateAdsPic() in AdvertisementDAO
			
			//set attribute to a servlet request
			request.setAttribute("ad", AdvertisementDAO.getAdById(adsid));
			request.setAttribute("getAlert", "UpdatePic Success");
		} 
		/* ----------------------------- END OF UPDATE ADVERTISEMENT -------------------------------- */
		
		
		/* -------------------------------- FORWARD THE REQUEST ------------------------------------------ */
		RequestDispatcher view = request.getRequestDispatcher(forward); 
		view.forward(request, response);
	}
}
