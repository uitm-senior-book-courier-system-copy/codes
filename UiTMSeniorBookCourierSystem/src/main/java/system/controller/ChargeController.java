package system.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
//import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import system.dao.ChargeDAO;
import system.model.Charge;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Servlet implementation class ChargeController
 */
public class ChargeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ChargeDAO chargedao; 
	private DateFormat formatter; 
	private HttpSession session;
	private String forward;
	private static String UPDATE = "AD_D_updateDelivery.jsp";
	int chargeid, empid;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChargeController() {
        super();
        chargedao = new ChargeDAO();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		/* ------------------------------- READ SINGLE DELIVERY CHARGE ----------------------------------- */
		forward = UPDATE;
		
		//set attribute to a servlet request 
		request.setAttribute("charge", ChargeDAO.getCharge());
		
		//forward the request
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		/* ------------------------------------ UPDATE DELIVERY CHARGE ------------------------------------ */
		try {
			
			Charge charge = new Charge();
			
			formatter = new SimpleDateFormat("yyyy-MM-dd"); 
			
			session = request.getSession(true);  
			empid = (Integer) session.getAttribute("id"); 
					
			charge.setChargeonorder(Double.parseDouble(request.getParameter("chargeonorder")));
			charge.setChargeontype(request.getParameter("chargeontype"));

			//get the date and convert to java date
			Date date1 = formatter.parse(request.getParameter("datechanged"));
			charge.setDatechanged(date1);
			
			charge.setEmpid(empid);	

			chargedao.addCharge(charge);

			//set attribute to a servlet request
			request.setAttribute("charge", ChargeDAO.getCharge()); 
			request.setAttribute("result","ucharge success"); 
			
		}catch(Exception e) {
			System.out.println(e);
		}
		
		forward = UPDATE; //forward the request
		
		RequestDispatcher view = request.getRequestDispatcher(forward);
		view.forward(request, response);
		
	}

}
