package system.controller;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
//import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import system.dao.OrderDAO;
import system.model.Order;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Servlet implementation class OrdersController
 */
@MultipartConfig(maxFileSize = 16777215)
public class OrderController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private OrderDAO orderdao; 
    private String action, forward;
    private int orderid, studid, empid;
    private HttpSession session;
    private DateFormat formatter; 
    
    //ADMIN
    private static String adminDashboard ="AD_B_dashboard.jsp";
    
    //STUDENT 
    private static String ST_VIEW_SHIPMENT_RESULT = "ST_E2_viewShipmentResult.jsp"; 
    private static String ST_VIEW_SHIPMENT = "ST_E1_viewShipment.jsp"; 
    
    //SENIOR 
  	private static String SE_LIST_SHIPMENT_REQUEST ="SE_E_shipmentRequest.jsp";
  	private static String SE_LIST_SHIPMENT = "SE_F_listShipmentStatus.jsp";
  	private static String SE_VIEW_EVIDENCE = "SE_G_editParcelEvidence.jsp";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OrderController() {
        super();
        orderdao = new OrderDAO(); 
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		session = request.getSession(true);  
		empid = (Integer) session.getAttribute("id"); 
		
		action = request.getParameter("action");
		
		
		/* --------------------------------------- ADMIN STATISTICS ----------------------------------- */
		if(action.equalsIgnoreCase("statistics"))
		{
			forward = adminDashboard;

			//get attribute to a servlet request
			request.setAttribute("adsDelivered", OrderDAO.getCountAdsDelivered());
			request.setAttribute("totalpriceDelivered", OrderDAO.getCountTPDelivered());
			request.setAttribute("stats", OrderDAO.getStatOnOrderDelivered());
		}
		/* ----------------------------------- END OF ADMIN STATISTICS ---------------------------------- */
		
		
		/* --------------------------------------- READ LIST SENIOR SHIPMENT ---------------------------- */
		//REQUEST (PENDING & REJECTED)
		if(action.equalsIgnoreCase("listshipmentrequest"))
		{
			forward = SE_LIST_SHIPMENT_REQUEST;

			//get attribute to a servlet request
			request.setAttribute("orders", OrderDAO.getPendingRejectedShipment(empid, "pending")); 
			request.setAttribute("ordersR", OrderDAO.getPendingRejectedShipment(empid, "rejected"));
		}
		
		//LIST (ACCEPTED, PREPARED, DELIVERED)
		if(action.equalsIgnoreCase("listshipment")) {
			
			forward = SE_LIST_SHIPMENT;

			//get attribute to a servlet request
			request.setAttribute("orders", OrderDAO.getMyShipments(empid));
		}
		/* ---------------------------------- END OF READ LIST SENIOR SHIPMENT ---------------------------- */
		
		
		/* --------------------------------- UPDATE STATUS OF ORDER -------------------------------------- */
		//"pending" -> "accepted"
		if(action.equalsIgnoreCase("accept"))
		{
			forward = SE_LIST_SHIPMENT_REQUEST;

			orderid = Integer.parseInt(request.getParameter("orderid"));

			orderdao.acceptRejectOrder(orderid, "accepted");

			//get attribute to a servlet request
			request.setAttribute("orders", OrderDAO.getPendingRejectedShipment(empid, "pending")); 
			request.setAttribute("ordersR", OrderDAO.getPendingRejectedShipment(empid, "rejected"));
			request.setAttribute("getAlert", "Accepted");
		}
		
		//"pending" -> "rejected"
		if(action.equalsIgnoreCase("reject"))
		{
			forward = SE_LIST_SHIPMENT_REQUEST;

			orderid = Integer.parseInt(request.getParameter("orderid"));

			orderdao.acceptRejectOrder(orderid, "rejected"); 

			//get attribute to a servlet request
			request.setAttribute("orders", OrderDAO.getPendingRejectedShipment(empid, "pending")); 
			request.setAttribute("ordersR", OrderDAO.getPendingRejectedShipment(empid, "rejected"));
			request.setAttribute("getAlert", "Rejected");
		}
		
		//"accepted" -> "prepared"
		//"prepared" -> "delivered"
		if(action.equalsIgnoreCase("updatestatus"))
		{	
			//retrieves values from html form
			orderid = Integer.parseInt(request.getParameter("orderid"));
			String status = request.getParameter("status");

			if(status.equalsIgnoreCase("accepted"))
			{
				//update status from "accepted" -> "prepared"
				orderdao.updateStatus(orderid, status); 
				
				request.setAttribute("getAlert", "Update to prepared");
			}
			else if(status.equalsIgnoreCase("prepared"))
			{
				//update status from "prepared" -> "delivered"
				orderdao.updateStatus(orderid, status); 
				
				request.setAttribute("getAlert", "Update to delivered");
			}

			forward = SE_LIST_SHIPMENT;

			request.setAttribute("orders", OrderDAO.getMyShipments(empid));
		}
		/* --------------------------------- END OF UPDATE STATUS OF ORDER -------------------------------- */
		
		
		/* --------------------------------- READ SINGLE ORDER - PARCEL EVIDENCE -------------------------- */
		if(action.equalsIgnoreCase("viewevidence")) {
	    	forward = SE_VIEW_EVIDENCE;
	    	
			orderid = Integer.parseInt(request.getParameter("orderid"));

			request.setAttribute("order", OrderDAO.getOrderShipment(orderid));
		}
		/* ---------------------------- END OF READ SINGLE ORDER - PARCEL EVIDENCE ------------------------ */
		
		
		/*----------------------------- READ LIST ORDER THAT STUDENT MADE ---------------------------------*/
		if(action.equalsIgnoreCase("studorderlist")) {
			
			session = request.getSession(true);
			studid = (Integer) session.getAttribute("id");
			
			request.setAttribute("orders", OrderDAO.getAllStudOrders(studid));
			forward = ST_VIEW_SHIPMENT;
		}
		/*----------------------------- END OF READ LIST ORDER THAT STUDENT MADE ---------------------------*/
		
		
		/* -------------------------------- FORWARD THE REQUEST ------------------------------------------ */
		RequestDispatcher view = request.getRequestDispatcher(forward); 
		view.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		action = request.getParameter("action");
		
		
		/* ------------------------------------------- ADD ORDER --------------------------------------- */
		//PARCEL EVIDENCE (SENIOR)
		if(action.equalsIgnoreCase("addevidence")) 
	    {
			//forward to G2_editParcelEvidenceSE.jsp
	    	forward = SE_VIEW_EVIDENCE;
	    	
			orderid = Integer.parseInt(request.getParameter("orderid"));
			
			Part evidencepic = request.getPart("evidencepic");
			
			//invoke method addEvidencepic() in OrderDAO
			orderdao.addEvidencepic(orderid, evidencepic);
			
			request.setAttribute("order", OrderDAO.getOrderShipment(orderid));
			request.setAttribute("getAlert", "Update Success");
		}
		
		//CHECKOUT ORDER (STUDENT)
		if(action.equalsIgnoreCase("addOrder")) {

			try {
				formatter = new SimpleDateFormat("yyyy-MM-dd");

				session = request.getSession(true);  
				studid = (Integer) session.getAttribute("id"); 

				Order order = new Order();

				Date date1 = formatter.parse(request.getParameter("orderdate")); //orderdate
				order.setOrderdate(date1);

				String typeofdelivery = request.getParameter("typeofdelivery"); //typeofdelivery
				order.setTypeofdelivery(typeofdelivery);

				Date date2 = formatter.parse(request.getParameter("mddate")); //mddate
				order.setMddate(date2);

				String rm1chargeOn = request.getParameter("rm1chargeOn");
				double totalprice = 0;

				//----- to check the totalprice with chargeontype: DELIVERY -----
				if(typeofdelivery.equalsIgnoreCase("delivery")) {
					order.setMdplace(request.getParameter("mdplaceD")); //mdplace; kolej

					if(rm1chargeOn.equalsIgnoreCase("delivery"))
						//if the charge is on delivery and stud choose type delivery
						totalprice = Double.parseDouble(request.getParameter("totalprice"));
					else
						//if the charge is on delivery but stud choose type meetup
						totalprice = Double.parseDouble(request.getParameter("totalprice"))-1;
				}

				//----- to check the totalprice with chargeontype: MEETUP -----
				if(typeofdelivery.equalsIgnoreCase("meetup")) {
					order.setMdplace(request.getParameter("mdplaceM")); //mdplace; medan selera/depan pb kolej

					if(rm1chargeOn.equalsIgnoreCase("meetup"))
						//if the charge is on meetup and stud choose type meetup
						totalprice = Double.parseDouble(request.getParameter("totalprice"));
					else
						//if the charge is on delivery but stud choose type delivery
						totalprice = Double.parseDouble(request.getParameter("totalprice"))-1;
				}

				order.setTotalprice(totalprice); //totalprice

				order.setMdtime(request.getParameter("mdtime"));

				order.setStudid(studid); //studid
				order.setAdsid(Integer.parseInt(request.getParameter("adsid")));

				orderdao.AddOrder(order);

				forward = ST_VIEW_SHIPMENT;

				request.setAttribute("orders", OrderDAO.getAllStudOrders(studid));
				request.setAttribute("result","Add Success"); 
			}
			catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		/* ------------------------------------- END OF ADD ORDER ------------------------------------- */
		
		
		/* -------------------------------- SEARCH STUDENT SHIPMENT  ---------------------------------- */
		if(action.equalsIgnoreCase("viewshipment")) {
			orderid = Integer.parseInt(request.getParameter("orderid"));
			
			boolean found = OrderDAO.checkOrderid(orderid, studid);
			
			//Check if orderid and studid exist or not
			if(found == true) { 
				forward = ST_VIEW_SHIPMENT_RESULT;
				
				request.setAttribute("order", OrderDAO.getOrderShipment(orderid));
			}
			else {
				forward = ST_VIEW_SHIPMENT;
				
				request.setAttribute("orders", OrderDAO.getAllStudOrders(studid));
				request.setAttribute("result","None"); 
			}
		}
		/* -------------------------------- END OF SEARCH STUDENT SHIPMENT  ------------------------------ */
		
		
		/* -------------------------------- FORWARD THE REQUEST ------------------------------------------ */
		RequestDispatcher view = request.getRequestDispatcher(forward); 
		view.forward(request, response);
		
	}

}
